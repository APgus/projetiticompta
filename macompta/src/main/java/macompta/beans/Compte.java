package macompta.beans;

public class Compte {

	private String NumeroDeCompte;
	private String libelleCompte;
	
	
	public Compte(String numeroDeCompte, String libelleCompte) {
		this.NumeroDeCompte = numeroDeCompte;
		this.libelleCompte = libelleCompte;
	}
	
	public String getNumeroDeCompte() {
		return NumeroDeCompte;
	}


	public void setNumeroDeCompte(String numeroDeCompte) {
		this.NumeroDeCompte = numeroDeCompte;
	}


	public String getLibelleCompte() {
		return libelleCompte;
	}


	public void setLibelleCompte(String libelleCompte) {
		this.libelleCompte = libelleCompte;
	}
	
}
