package macompta.beans;

public class Entreprise {
	private Integer idEntreprise;
	private String login;
	private String motDePasse;
	private String nom;
	private String adresse;
	private String codePostal;
	private String ville;
	private String telephone;
	private String secteurDActivite;
	
	
	public Entreprise(Integer idEntreprise,String login, String motDePasse, String nom,
			String adresse, String codePostal, String ville, String telephone,
			String secteurDActivite) {
		this.idEntreprise = idEntreprise;
		this.login = login;
		this.motDePasse = motDePasse;
		this.nom = nom;
		this.adresse = adresse;
		this.codePostal = codePostal;
		this.ville = ville;
		this.telephone = telephone;
		this.secteurDActivite = secteurDActivite;
	}
	
	
	
	public Entreprise(String login, String motDePasse) {
		this.login = login;
		this.motDePasse = motDePasse;
	}



	public Integer getIdEntreprise() {
		return idEntreprise;
	}
	public  void setIdEntreprise(Integer idEntreprise) {
		this.idEntreprise = idEntreprise;
	}
	public String getLogin() {
		return login;
	}
	public  void setLogin(String login) {
		this.login = login;
	}
	public String getMotDePasse() {
		return motDePasse;
	}
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getSecteurDActivite() {
		return secteurDActivite;
	}
	public void setSecteurDActivite(String secteurDActivite) {
		this.secteurDActivite = secteurDActivite;
	}
	
}
