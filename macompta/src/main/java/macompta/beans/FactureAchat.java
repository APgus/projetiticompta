package macompta.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FactureAchat {
	
	private static DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

	private Integer idAchat;
	private Date dateAchat;
	private String nomAFournisseur;
	private String libelleFacture;
	private float montantAHT;
	private float tvaA;
	private Date dateAGenerationEXCEL;
	private int idEntrepriseLiée;
	private String numeroDeCompte;
	private String modePaiement;
	
	
	
	public FactureAchat(Integer idAchat,Date dateAchat, String nomAFournisseur,String libelleFacture,float montantAHT, float tvaA, Date dateAGenerationEXCEL, int idEntrepriseLiée, String numeroDeCompte, String modePaiement){
		this.idAchat=idAchat;
		this.dateAchat=dateAchat;
		this.nomAFournisseur=nomAFournisseur;
		this.libelleFacture=libelleFacture;
		this.montantAHT=montantAHT;
		this.tvaA=tvaA;
		this.dateAGenerationEXCEL=dateAGenerationEXCEL;
		this.idEntrepriseLiée=idEntrepriseLiée;
		this.numeroDeCompte=numeroDeCompte;
		this.modePaiement=modePaiement;
	}
	
	public Integer getIdAchat() {
		return idAchat;
	}
	
	public void setIdAchat(Integer idAchat){
		this.idAchat=idAchat;
	}

	public Date getDateAchat() {
		return dateAchat;
	}
	
	public void setDateAchat(Date dateAchat){
		this.dateAchat=dateAchat;
	}
	
		
	public String getDateAchatFormatee() {
		return dateFormat.format(dateAchat);
	}
	
	public String getNomAFournisseur() {
		return nomAFournisseur;
	}
	
	public void setNomFournisseur(String nomAFournisseur){
		this.nomAFournisseur=nomAFournisseur;
	}
	public float getMontantAHT() {
		return montantAHT;
	}
	
	public void setMontantAHT(float montantAHT){
		this.montantAHT=montantAHT;
	}
	public float getTvaA() {
		return tvaA;
	}
	
	public void setTvaA(float tvaA){
		this.tvaA=tvaA;
	}
	public Date getDateAGenerationEXCEL() {
		return dateAGenerationEXCEL;
	}
	
	public void setDateAGenerationEXCEL(Date dateAGenerationEXCEL){
		this.dateAGenerationEXCEL=dateAGenerationEXCEL;
	}
	
	public String getDateAGenerationEXCELFormatee() {
		return dateFormat.format(dateAGenerationEXCEL);
	}
	
	public int getIdEntrepriseLiée() {
		return idEntrepriseLiée;
	}
	
	public void setIdEntrepriseLiée(int idEntrepriseLiée){
		this.idEntrepriseLiée=idEntrepriseLiée;
	}
	public String getNumeroDeCompte() {
		return numeroDeCompte;
	}
	
	public void setNumeroDeCompte(String numeroDeCompte){
		this.numeroDeCompte=numeroDeCompte;
	}

	public String getLibelleFacture() {
		return libelleFacture;
	}

	public void setLibelleFacture(String libelleFacture) {
		this.libelleFacture = libelleFacture;
	}

	public String getModePaiement() {
		return modePaiement;
	}

	public void setModePaiement(String modePaiement) {
		this.modePaiement = modePaiement;
	}

}
