package macompta.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FactureAchatImmobilisation {

	private static DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
	
	private Integer idImmo;
	private Date dateImmo;
	private String nomIFournisseur;
	private String libelleFacture;
	private float montantIHT;
	private float tvaI;
	private Date dateIGenerationEXCEL;
	private int idEntrepriseLiée;
	private String numeroDeCompte;
	private Float tauxAmort;
	private String modePaiement;
	
	
	public FactureAchatImmobilisation(Integer idImmo,Date dateImmo, String nomIFournisseur,String libelleFacture,float montantIHT, float tvaI, Date dateIGenerationEXCEL, int idEntrepriseLiée, String numeroDeCompte,Float tauxAmort,  String modePaiement){
		this.idImmo=idImmo;
		this.dateImmo=dateImmo;
		this.nomIFournisseur=nomIFournisseur;
		this.libelleFacture=libelleFacture;
		this.montantIHT=montantIHT;
		this.tvaI=tvaI;
		this.dateIGenerationEXCEL=dateIGenerationEXCEL;
		this.idEntrepriseLiée=idEntrepriseLiée;
		this.numeroDeCompte=numeroDeCompte;
		this.tauxAmort=tauxAmort;
		this.modePaiement=modePaiement;
	}
	
	public Integer getIdImmo() {
		return idImmo;
	}
	
	public void setIdImmo(Integer idImmo){
		this.idImmo=idImmo;
	}

	public Date getDateImmo() {
		return dateImmo;
	}
	
	public void setDateImmo(Date dateImmo){
		this.dateImmo=dateImmo;
	}
	
	public String getDateImmoFormatee() {
		return dateFormat.format(dateImmo);
	}
	
	public String getNomIFournisseur() {
		return nomIFournisseur;
	}
	
	public void setNomIFournisseur(String nomIFournisseur){
		this.nomIFournisseur=nomIFournisseur;
	}
	public float getMontantIHT() {
		return montantIHT;
	}
	
	public void setMontantIHT(float montantIHT){
		this.montantIHT=montantIHT;
	}
	public float getTvaI() {
		return tvaI;
	}
	
	public void setTvaI(float tvaI){
		this.tvaI=tvaI;
	}
	public Date getDateIGenerationEXCEL() {
		return dateIGenerationEXCEL;
	}
	
	public void setDateIGenerationEXCEL(Date dateIGenerationEXCEL){
		this.dateIGenerationEXCEL=dateIGenerationEXCEL;
	}
	
	public String getDateIGenerationEXCELFormatee() {
		return dateFormat.format(dateIGenerationEXCEL);
	}

	public int getIdEntrepriseLiée() {
		return idEntrepriseLiée;
	}
	
	public void setIdEntrepriseLiée(int idEntrepriseLiée){
		this.idEntrepriseLiée=idEntrepriseLiée;
	}
	public String getNumeroDeCompte() {
		return numeroDeCompte;
	}
	
	public void setNumeroDeCompte(String numeroDeCompte){
		this.numeroDeCompte=numeroDeCompte;
	}

	public Float getTauxAmort() {
		return tauxAmort;
	}

	public void setTauxAmort(Float tauxAmort) {
		this.tauxAmort = tauxAmort;
	}

	public String getLibelleFacture() {
		return libelleFacture;
	}

	public void setLibelleFacture(String libelleFacture) {
		this.libelleFacture = libelleFacture;
	}

	public String getModePaiement() {
		return modePaiement;
	}

	public void setModePaiement(String modePaiement) {
		this.modePaiement = modePaiement;
	}
	
}
