package macompta.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FactureVente {
	
	private static DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

	private Integer idVente;
	private Date dateVente;
	private String nomClient;
	private String libelleFacture;
	private float montantVHT;
	private float tvaV;
	private Date dateVGenerationEXCEL;
	private int idEntrepriseLiée;
	private String numeroDeCompte;
	private String modePaiement;

	
	
	public FactureVente(Integer idVente,Date dateVente, String nomClient,String libelleFacture,float montantVHT, float tvaV, Date dateVGenerationEXCEL, int idEntrepriseLiée, String numeroDeCompte, String modePaiement){
		this.idVente=idVente;
		this.dateVente=dateVente;
		this.nomClient=nomClient;
		this.libelleFacture=libelleFacture;
		this.montantVHT=montantVHT;
		this.tvaV=tvaV;
		this.dateVGenerationEXCEL=dateVGenerationEXCEL;
		this.idEntrepriseLiée=idEntrepriseLiée;
		this.numeroDeCompte=numeroDeCompte;
		this.modePaiement=modePaiement;
	}
	
	public Integer getIdVente() {
		return idVente;
	}


	public void setIdVente(Integer idVente) {
		this.idVente = idVente;
	}


	public Date getDateVente() {
		return dateVente;
	}


	public void setDateVente(Date dateVente) {
		this.dateVente = dateVente;
	}

	public String getDateVenteFormatee() {
		return dateFormat.format(dateVente);
	}
	

	public String getNomClient() {
		return nomClient;
	}


	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}


	public float getMontantVHT() {
		return montantVHT;
	}


	public void setMontantVHT(float montantVHT) {
		this.montantVHT = montantVHT;
	}


	public float getTvaV() {
		return tvaV;
	}


	public void setTvaV(float tvaV) {
		this.tvaV = tvaV;
	}


	public Date getDateVGenerationEXCEL() {
		return dateVGenerationEXCEL;
	}


	public void setDateVGenerationEXCEL(Date dateVGenerationEXCEL) {
		this.dateVGenerationEXCEL = dateVGenerationEXCEL;
	}

	public String getDateVGenerationEXCELFormatee() {
		return dateFormat.format(dateVGenerationEXCEL);
	}
		

	public int getIdEntrepriseLiée() {
		return idEntrepriseLiée;
	}


	public void setIdEntrepriseLiée(int idEntrepriseLiée) {
		this.idEntrepriseLiée = idEntrepriseLiée;
	}


	public String getNumeroDeCompte() {
		return numeroDeCompte;
	}


	public void setNumeroDeCompte(String numeroDeCompte) {
		this.numeroDeCompte = numeroDeCompte;
	}
	
	
	
	public String getLibelleFacture() {
		return libelleFacture;
	}

	public void setLibelleFacture(String libelleFacture) {
		this.libelleFacture = libelleFacture;
	}

	
	public String getModePaiement() {
		return modePaiement;
	}


	public void setModePaiement(String modePaiement) {
		this.modePaiement = modePaiement;
	}
	

	
}
