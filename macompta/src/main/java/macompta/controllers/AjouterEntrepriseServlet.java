package macompta.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import macompta.beans.Entreprise;
import macompta.metier.Manager;

/**
 * Servlet implementation class AjouterEntrepriseServlet
 */
public class AjouterEntrepriseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjouterEntrepriseServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/ajouterentreprise.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String nom = request.getParameter("nom");
		String login = request.getParameter("login");
		String motDePasse = request.getParameter("motDePasse");
		String adresse = request.getParameter("adresse");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String telephone = request.getParameter("telephone");
		String secteurDActivite = request.getParameter("secteurDActivite");
		
		
		Entreprise nouvelleentreprise = new Entreprise(null,login,motDePasse,nom,adresse,codePostal,ville,telephone,secteurDActivite);
		Entreprise nouvelleentrepriseatester = new Entreprise(login,motDePasse);
		if (!(Manager.getInstance().EntrepriseExiste(nouvelleentrepriseatester))){
			Manager.getInstance().ajouterEntreprise(nouvelleentreprise);
			response.sendRedirect("ajouterentreprise");
		}
		else
		response.sendRedirect("ajouterentreprise");
	}
		
}
