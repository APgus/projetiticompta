package macompta.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import macompta.beans.Compte;
import macompta.beans.Entreprise;
import macompta.metier.Manager;


public class ConnexionServlet extends HttpServlet {
        private static final long serialVersionUID = 1L;

        public ConnexionServlet() {
    		super();
    		// TODO Auto-generated constructor stub
    	}
        
        public void init() throws ServletException {
    		super.init();

    	}

        public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        		// TODO Auto-generated method stub
        		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/connexion.jsp");
        		view.forward(request, response);
        }
        
        

        public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        	// TODO Auto-generated method stub
        	
        	String login = request.getParameter("login");
    		String motDePasse = request.getParameter("motDePasse");
    		Entreprise entreprise = new Entreprise(login, motDePasse);
    		if (Manager.getInstance().EntrepriseExiste(entreprise)) 
    		{
    			HttpSession session = request.getSession(true);
    			Entreprise entrepriseenligne = Manager.getInstance().RecupererEntreprise(login);
    			session.setAttribute("entrepriseConnectee", entrepriseenligne);
    			session.setAttribute("entrepriseConnecteeBis", entrepriseenligne);
    			List<Compte> listecompte = Manager.getInstance().listerCompte();
    			session.setAttribute("listecompte", listecompte);
    			
    			String successconnexion = "Vous �tes connect� au site !";
    			session.setAttribute("successconnexion", successconnexion);	
    			
                response.sendRedirect("accueil");
    		}
    		else 
    		{
    			request.setAttribute("erreurconnexion", "Votre identifiant ou votre mot de passe n'est pas valide. Veuillez saisir des valeurs correctes.");
				RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/connexion.jsp");
				view.forward(request, response);
    			
    		} 
    		
    		
        }
}    