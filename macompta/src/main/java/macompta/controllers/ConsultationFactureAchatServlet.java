package macompta.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import macompta.beans.Entreprise;
import macompta.beans.FactureAchat;
import macompta.metier.Manager;

/**
 * Servlet implementation class ConsultationFactureAchatServlet
 */
public class ConsultationFactureAchatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultationFactureAchatServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<FactureAchat> listefactureachat = Manager.getInstance().listerFactureAchat((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
		request.setAttribute("listefactureachat", listefactureachat);
		

		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/consultationFactureAchat.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Date dateinferieureachat = null;
		if(!"".equals(request.getParameter("dateinferieureachat"))) {
			try {
				dateinferieureachat = dateFormat.parse(request.getParameter("dateinferieureachat"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		
		Date datesuperieureachat = null;
		if(!"".equals(request.getParameter("datesuperieureachat"))) {
			try {
				datesuperieureachat = dateFormat.parse(request.getParameter("datesuperieureachat"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		
		HttpSession session = request.getSession();
		session.setAttribute("dateinferieureachat", dateinferieureachat);
		session.setAttribute("datesuperieureachat", datesuperieureachat);
		
		response.sendRedirect("consultationFactureAchat");
		
	}

}
