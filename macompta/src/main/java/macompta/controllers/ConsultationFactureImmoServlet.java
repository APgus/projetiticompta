package macompta.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import macompta.beans.Entreprise;
import macompta.beans.FactureAchatImmobilisation;
import macompta.metier.Manager;

/**
 * Servlet implementation class ConsultationFactureImmoServlet
 */
public class ConsultationFactureImmoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultationFactureImmoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<FactureAchatImmobilisation> listefactureimmo = Manager.getInstance().listerFactureImmo((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
		request.setAttribute("listefactureimmo", listefactureimmo);		
		
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/consultationFactureImmo.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Date dateinferieureimmo = null;
		if(!"".equals(request.getParameter("dateinferieureimmo"))) {
			try {
				dateinferieureimmo = dateFormat.parse(request.getParameter("dateinferieureimmo"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		
		Date datesuperieureimmo = null;
		if(!"".equals(request.getParameter("datesuperieureimmo"))) {
			try {
				datesuperieureimmo = dateFormat.parse(request.getParameter("datesuperieureimmo"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		
		HttpSession session = request.getSession();
		session.setAttribute("dateinferieureimmo", dateinferieureimmo);
		session.setAttribute("datesuperieureimmo", datesuperieureimmo);
		
		response.sendRedirect("consultationFactureImmo");
		
	}

}
