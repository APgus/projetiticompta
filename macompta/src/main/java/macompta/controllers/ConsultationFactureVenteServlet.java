package macompta.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import macompta.beans.Entreprise;
import macompta.beans.FactureVente;
import macompta.metier.Manager;

/**
 * Servlet implementation class ConsultationFactureVenteServlet
 */
public class ConsultationFactureVenteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultationFactureVenteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<FactureVente> listefacturevente = Manager.getInstance().listerFactureVente((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
		request.setAttribute("listefacturevente", listefacturevente);		
		
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/consultationFactureVente.jsp");
		view.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Date dateinferieurevente = null;
		if(!"".equals(request.getParameter("dateinferieurevente"))) {
			try {
				dateinferieurevente = dateFormat.parse(request.getParameter("dateinferieurevente"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		
		Date datesuperieurevente = null;
		if(!"".equals(request.getParameter("datesuperieurevente"))) {
			try {
				datesuperieurevente = dateFormat.parse(request.getParameter("datesuperieurevente"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		
		HttpSession session = request.getSession();
		session.setAttribute("dateinferieurevente", dateinferieurevente);
		session.setAttribute("datesuperieurevente", datesuperieurevente);
		
		response.sendRedirect("consultationFactureVente");
		
	}

}
