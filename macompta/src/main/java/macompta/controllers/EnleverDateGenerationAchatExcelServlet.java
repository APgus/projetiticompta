package macompta.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import macompta.beans.FactureAchat;
import macompta.metier.Manager;

/**
 * Servlet implementation class EnleverDateGenerationAchatExcelServlet
 */
public class EnleverDateGenerationAchatExcelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EnleverDateGenerationAchatExcelServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Integer idAchat = Integer.parseInt(request.getParameter("idAchat"));
		Manager.getInstance().enleverDateGenerationExcel(new FactureAchat(idAchat,null, "","", 0, 0, null, 0, null, null));
		response.sendRedirect("consultationFactureAchat");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
