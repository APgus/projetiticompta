package macompta.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import macompta.beans.FactureAchatImmobilisation;
import macompta.metier.Manager;

/**
 * Servlet implementation class EnleverDateGenerationImmoServlet
 */
public class EnleverDateGenerationImmoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EnleverDateGenerationImmoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Integer idImmo = Integer.parseInt(request.getParameter("idImmo"));
		Manager.getInstance().enleverDateGenerationExcel(new FactureAchatImmobilisation(idImmo,null, "","", 0, 0, null, 0, null, null, null));
		response.sendRedirect("consultationFactureImmo");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
