package macompta.controllers;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import macompta.beans.Entreprise;
import macompta.beans.FactureVente;
import macompta.metier.Manager;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.Region;

/**
 * Servlet implementation class GenerationExcelVenteServlet
 */
public class GenerationExcelVenteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 public void init(ServletConfig config) throws ServletException {
	        super.init(config);  
	    }

	    public void destroy() {
	    }

	    /** Processes requests for both HTTP GET and POST methods.
	     * @param request servlet request
	     * @param response servlet response
	     */

	    protected void processRequest(HttpServletRequest request,
	        HttpServletResponse response) throws ServletException, IOException {
	    	
	    	
	    	List<FactureVente> listefactureventeagenerer = Manager.getInstance().listerFactureVenteAGenerer((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
			request.setAttribute("listefactureventeagenerer", listefactureventeagenerer);		
	    	
	    	DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
	    	Date date = new Date();
	    	String datef = dateFormat.format(date);
	    	String filename = datef + " factures de vente.xls";
	    	OutputStream out = null;
	    	try
	    	{
		        response.setContentType("application/vnd.ms-excel");
		        response.setHeader("Content-disposition", "attachment; filename="+ filename);
		        HSSFWorkbook wb = new HSSFWorkbook();
		        HSSFSheet sheet = wb.createSheet("Factures de vente");
		        
		        
		        HSSFRow row0 ;
		        row0 = sheet.createRow((short)0);
		        
		        for(int i=0;i<8;i++){
		        
			    sheet.addMergedRegion(new Region(0,(short)i,1,(short)i));
			    
		        }
		        
		        row0.createCell((short)0).setCellValue("VE"); 
		        row0.createCell((short)1).setCellValue("Num Compte"); 
		        row0.createCell((short)2).setCellValue("Date"); 
		        row0.createCell((short)3).setCellValue("Libelles"); 
		        row0.createCell((short)4).setCellValue("TVA"); 
		        row0.createCell((short)5).setCellValue("DEBIT"); 
		        row0.createCell((short)6).setCellValue("CREDIT"); 
		        row0.createCell((short)7).setCellValue("Mode Paiement"); 
		        
		        
		        for(int i=0; i<listefactureventeagenerer.size();i=i+1){
		        	for(int j=2+3*(i); j<listefactureventeagenerer.size()*3;j=j+3){
				        // Create a row and put some cells in it. Rows are 0 based.
				        HSSFRow row1 ;   
				        row1 = sheet.createRow((short)j);
				        HSSFRow row2  ;  
				        row2 = sheet.createRow((short)j+1);
				        HSSFRow row3  ;  
				        row3 = sheet.createRow((short)j+2);

				        row1.createCell((short)0).setCellValue("VE"); 
				        row1.createCell((short)1).setCellValue("700000");
				        row1.createCell((short)2).setCellValue(listefactureventeagenerer.get(i).getDateVenteFormatee());
				        row1.createCell((short)3).setCellValue("Recettes : " + listefactureventeagenerer.get(i).getLibelleFacture());
				        row1.createCell((short)4).setCellValue("");
				        row1.createCell((short)5).setCellValue("");
				        row1.createCell((short)6).setCellValue(listefactureventeagenerer.get(i).getMontantVHT());
				        row1.createCell((short)7).setCellValue("");
				        
				        row2.createCell((short)0).setCellValue("VE"); 
				        row2.createCell((short)1).setCellValue("445700");
				        row2.createCell((short)2).setCellValue(listefactureventeagenerer.get(i).getDateVenteFormatee());
				        row2.createCell((short)3).setCellValue("Tva sur Recettes : "+ listefactureventeagenerer.get(i).getLibelleFacture());
				        row2.createCell((short)4).setCellValue("");
				        row2.createCell((short)5).setCellValue("");
				        row2.createCell((short)6).setCellValue(listefactureventeagenerer.get(i).getTvaV());
				        row2.createCell((short)7).setCellValue("");
				        
				        row3.createCell((short)0).setCellValue("VE"); 
				        row3.createCell((short)1).setCellValue("530000");
				        row3.createCell((short)2).setCellValue(listefactureventeagenerer.get(i).getDateVenteFormatee());
				        row3.createCell((short)3).setCellValue("Recettes : " + listefactureventeagenerer.get(i).getLibelleFacture());
				        row3.createCell((short)4).setCellValue("");
				        row3.createCell((short)5).setCellValue(listefactureventeagenerer.get(i).getMontantVHT()*(100+listefactureventeagenerer.get(i).getTvaV())/100);
				        row3.createCell((short)6).setCellValue("");
				        row3.createCell((short)7).setCellValue(listefactureventeagenerer.get(i).getModePaiement());
		        	}
			     }
		        
		        
		        sheet.autoSizeColumn(1, true);
		        sheet.autoSizeColumn(2, true);
		        sheet.autoSizeColumn(3, true);
		        sheet.autoSizeColumn(7, true);
		        // Write the output 
		        out = response.getOutputStream();
		        wb.write(out);
		        out.close();
	    	} 
	    	catch (Exception e) {
	    	     throw new ServletException("Exception in Excel Sample Servlet", e);
	    	} 
	    	finally {
	    	     if (out != null)
	    	      out.close();
	    	}  
		     
	    	for(int i=0; i<listefactureventeagenerer.size();i=i+1){
	    		listefactureventeagenerer.get(i).setDateVGenerationEXCEL(date);
	    	Manager.getInstance().validerGenerationExcel(listefactureventeagenerer.get(i),(Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
	    	
	    	}
	    }

	    /** Handles the HTTP <code>GET</code> method.
	     * @param request servlet request
	     * @param response servlet response
	     */

	    protected void doGet(HttpServletRequest request,
	        HttpServletResponse response) throws ServletException, IOException {
	        processRequest(request, response);
	    }

	    /** Handles the HTTP POST method.
	     * @param request servlet request
	     * @param response servlet response
	     */

	    protected void doPost(HttpServletRequest request,
	        HttpServletResponse response) throws ServletException, IOException {
	        processRequest(request, response);
	    }

	    /** Returns a short description of the servlet.
	     */

	    public String getServletInfo() {
	       return "Example to create a workbook in a servlet using HSSF";
	    }
	}