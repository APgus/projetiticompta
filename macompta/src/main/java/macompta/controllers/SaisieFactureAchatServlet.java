package macompta.controllers;


import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import macompta.beans.Entreprise;
import macompta.beans.FactureAchat;
import macompta.metier.Manager;

/**
 * Servlet implementation class SaisieFactureAchat
 */
public class SaisieFactureAchatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaisieFactureAchatServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		FactureAchat dernierefactureachat = Manager.getInstance().AfficherDerniereFactureAchats((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
		request.setAttribute("dernierefactureachat", dernierefactureachat);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/saisieFactureAchat.jsp");
		view.forward(request, response);

	}
	


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nomAFournisseur = request.getParameter("nomAFournisseur");
		String libelleFacture = request.getParameter("libelleFacture");
		int idEntrepriseLiée = Integer.parseInt(request.getParameter("idEntrepriseLiée"));
		String numeroDeCompte = request.getParameter("numeroDeCompte");
		String modePaiement = request.getParameter("modePaiement");
		Float montantAHT = Float.parseFloat(request.getParameter("montantAHT"));
		Float tvaA = Float.parseFloat(request.getParameter("tvaA"));
		
		Date dateAchat = null;
		if(!"".equals(request.getParameter("dateAchat"))) {
			try {
				dateAchat = dateFormat.parse(request.getParameter("dateAchat"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
			
		Date dateGenerationEXCEL = null;
		
		FactureAchat nouvelleFactureAchat = new FactureAchat(null, dateAchat, nomAFournisseur, libelleFacture, montantAHT,tvaA ,dateGenerationEXCEL, idEntrepriseLiée ,numeroDeCompte ,modePaiement);
		Manager.getInstance().ajouterFactureAchat(nouvelleFactureAchat);
		
		response.sendRedirect("saisieFactureAchat");
	}
		
}
