package macompta.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import macompta.beans.Entreprise;
import macompta.beans.FactureAchatImmobilisation;
import macompta.metier.Manager;

/**
 * Servlet implementation class SaisieFactureImmo
 */
public class SaisieFactureImmoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaisieFactureImmoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	FactureAchatImmobilisation dernierefactureachatimmo = Manager.getInstance().AfficherDerniereFactureAchatsImmo((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
		request.setAttribute("dernierefactureachatimmo", dernierefactureachatimmo);
    	
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/saisieFactureImmo.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nomIFournisseur = request.getParameter("nomIFournisseur");
		String libelleFacture = request.getParameter("libelleFacture");
		int idEntrepriseLiée = Integer.parseInt(request.getParameter("idEntrepriseLiée"));
		String numeroDeCompte = request.getParameter("numeroDeCompte");
		String modePaiement = request.getParameter("modePaiement");
		Float tauxAmort =  Float.parseFloat(request.getParameter("tauxAmort"));
		Float montantIHT = Float.parseFloat(request.getParameter("montantIHT"));
		Float tvaI = Float.parseFloat(request.getParameter("tvaI"));
		
		Date dateImmo = null;
		if(!"".equals(request.getParameter("dateImmo"))) {
			try {
				dateImmo = dateFormat.parse(request.getParameter("dateImmo"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
			
		Date dateGenerationEXCEL = null;
		
		FactureAchatImmobilisation nouvelleFactureAchatImmobilisation = new FactureAchatImmobilisation(null, dateImmo, nomIFournisseur, libelleFacture, montantIHT,tvaI ,dateGenerationEXCEL, idEntrepriseLiée ,numeroDeCompte, tauxAmort,modePaiement);
		Manager.getInstance().ajouterFactureAchatImmobilisation(nouvelleFactureAchatImmobilisation);
		
		response.sendRedirect("saisieFactureImmo");
	}
}
