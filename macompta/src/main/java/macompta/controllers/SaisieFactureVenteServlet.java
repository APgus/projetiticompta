package macompta.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import macompta.beans.Entreprise;
import macompta.beans.FactureVente;
import macompta.metier.Manager;

/**
 * Servlet implementation class SaisieFactureVenteServlet
 */
public class SaisieFactureVenteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaisieFactureVenteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		FactureVente dernierefacturevente = Manager.getInstance().AfficherDerniereFactureVentes((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
		request.setAttribute("dernierefacturevente", dernierefacturevente);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/saisieFactureVente.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String nomClient = request.getParameter("nomClient");
		String libelleFacture = request.getParameter("libelleFacture");
		int idEntrepriseLiée = Integer.parseInt(request.getParameter("idEntrepriseLiée"));
		String numeroDeCompte = request.getParameter("numeroDeCompte");
		String modePaiement = request.getParameter("modePaiement");
		Float montantVHT = Float.parseFloat(request.getParameter("montantVHT"));
		Float tvaV = Float.parseFloat(request.getParameter("tvaV"));
		
		Date dateVente = null;
		if(!"".equals(request.getParameter("dateVente"))) {
			try {
				dateVente = dateFormat.parse(request.getParameter("dateVente"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
			
		Date dateGenerationEXCEL = null;
		
		FactureVente nouvelleFactureVente = new FactureVente(null, dateVente, nomClient, libelleFacture, montantVHT,tvaV ,dateGenerationEXCEL,idEntrepriseLiée ,numeroDeCompte ,modePaiement);
		Manager.getInstance().ajouterFactureVente(nouvelleFactureVente);
		
		response.sendRedirect("saisieFactureVente");
	}

}
