package macompta.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import macompta.beans.Compte;
import macompta.beans.Entreprise;
import macompta.metier.Manager;

/**
 * Servlet implementation class SeConnecterEnTantQueServlet
 */
public class SeConnecterEnTantQueServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SeConnecterEnTantQueServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Entreprise> listeentreprise = Manager.getInstance().listerEntreprise();
		request.setAttribute("listeentreprise", listeentreprise);	
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/seconnecterentantque.jsp");
		view.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String login = request.getParameter("login");
		
		HttpSession session = request.getSession();
		Entreprise entrepriseAdmin = (Entreprise) request.getSession().getAttribute("entrepriseConnecteeBis");
		
	    session.invalidate();
	    
	    session = request.getSession(true);
		Entreprise entrepriseenligne = Manager.getInstance().RecupererEntreprise(login);
		session.setAttribute("entrepriseConnectee", entrepriseenligne);
		session.setAttribute("entrepriseConnecteeBis", entrepriseAdmin);
		List<Compte> listecompte = Manager.getInstance().listerCompte();
		session.setAttribute("listecompte", listecompte);		
        response.sendRedirect("accueil");
		
	}

}
