package macompta.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import macompta.beans.Entreprise;
import macompta.beans.FactureAchat;
import macompta.beans.FactureAchatImmobilisation;
import macompta.beans.FactureVente;
import macompta.metier.Manager;

/**
 * Servlet implementation class TvaAnuelleServlet
 */
public class TvaAnuelleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TvaAnuelleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		List<FactureAchat> listefactureachat = Manager.getInstance().listerFactureAchat((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
		request.setAttribute("listefactureachat", listefactureachat);
		
		List<FactureVente> listefacturevente = Manager.getInstance().listerFactureVente((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
		request.setAttribute("listefacturevente", listefacturevente);		
		
		List<FactureAchatImmobilisation> listefactureimmo = Manager.getInstance().listerFactureImmo((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
		request.setAttribute("listefactureimmo", listefactureimmo);	

		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/tvaAnuelle.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Date anneetvainf = null;
		if(!"".equals(request.getParameter("anneetvainf"))) {
			try {
				anneetvainf = dateFormat.parse(request.getParameter("anneetvainf"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		
		Date anneetvasup = null;
		if(!"".equals(request.getParameter("anneetvasup"))) {
			try {
				anneetvasup = dateFormat.parse(request.getParameter("anneetvasup"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		
		HttpSession session = request.getSession();
		session.setAttribute("anneetvainf", anneetvainf);
		session.setAttribute("anneetvasup", anneetvasup);
		
		response.sendRedirect("tvaAnuelle");
	}

}
