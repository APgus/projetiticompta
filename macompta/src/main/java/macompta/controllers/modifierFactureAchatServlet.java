package macompta.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import macompta.beans.Entreprise;
import macompta.beans.FactureAchat;
import macompta.metier.Manager;

/**
 * Servlet implementation class modifierFactureAchatServlet
 */
public class modifierFactureAchatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public modifierFactureAchatServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int idAchat = Integer.parseInt(request.getParameter("idAchat"));  
		  
		 FactureAchat factureachat =Manager.getInstance().getFactureAchatById(idAchat);  

         request.setAttribute("factureachat", factureachat);  

		
		List<FactureAchat> factureachats = Manager.getInstance().listerFactureAchat((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
		request.setAttribute("factureachats", factureachats);

		
		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/pages/modifierFactureAchat.jsp");
		view.forward(request, response);
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		FactureAchat nouvellefactureachat = new FactureAchat(null, null, null, null, 0, 0, null, 0, null, null);
		Integer idAchat = Integer.parseInt(request.getParameter("idAchat"));
		nouvellefactureachat.setIdAchat(idAchat);  
		try {
			nouvellefactureachat.setDateAchat(dateFormat.parse(request.getParameter("dateAchat")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		nouvellefactureachat.setNomFournisseur(request.getParameter("nomAFournisseur"));
		nouvellefactureachat.setLibelleFacture(request.getParameter("libelleFacture"));
		nouvellefactureachat.setMontantAHT(Float.parseFloat(request.getParameter("montantAHT")));
		nouvellefactureachat.setTvaA(Float.parseFloat(request.getParameter("tvaA")));
		try {
			nouvellefactureachat.setDateAGenerationEXCEL(dateFormat.parse(request.getParameter("dateAGenerationEXCEL")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		nouvellefactureachat.setIdEntrepriseLiée(Integer.parseInt(request.getParameter("idEntrepriseLiée")));
		nouvellefactureachat.setNumeroDeCompte(request.getParameter("numeroDeCompte"));
		nouvellefactureachat.setModePaiement(request.getParameter("modePaiement"));
	

		Manager.getInstance().modifierFactureAchat(nouvellefactureachat);
		
		response.sendRedirect("consultationFactureAchat");  
		
	}

}
