package macompta.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import macompta.beans.Entreprise;
import macompta.beans.FactureAchatImmobilisation;
import macompta.metier.Manager;

/**
 * Servlet implementation class modifierFactureImmoServlet
 */
public class modifierFactureImmoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public modifierFactureImmoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idImmo = Integer.parseInt(request.getParameter("idImmo"));  
		  
		 FactureAchatImmobilisation factureimmo =Manager.getInstance().getFactureImmoById(idImmo);  

        request.setAttribute("factureimmo", factureimmo);  

		
		List<FactureAchatImmobilisation> factureimmos = Manager.getInstance().listerFactureImmo((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
		request.setAttribute("factureimmos", factureimmos);

		
		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/pages/modifierFactureImmo.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		FactureAchatImmobilisation nouvellefactureimmo = new FactureAchatImmobilisation(null, null, null, null, 0, 0, null, 0, null, null, null);
		Integer idImmo = Integer.parseInt(request.getParameter("idImmo"));
		nouvellefactureimmo.setIdImmo(idImmo);  
		try {
			nouvellefactureimmo.setDateImmo(dateFormat.parse(request.getParameter("dateImmo")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		nouvellefactureimmo.setNomIFournisseur(request.getParameter("nomIFournisseur"));
		nouvellefactureimmo.setLibelleFacture(request.getParameter("libelleFacture"));
		nouvellefactureimmo.setMontantIHT(Float.parseFloat(request.getParameter("montantIHT")));
		nouvellefactureimmo.setTvaI(Float.parseFloat(request.getParameter("tvaI")));
		try {
			nouvellefactureimmo.setDateIGenerationEXCEL(dateFormat.parse(request.getParameter("dateIGenerationEXCEL")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		nouvellefactureimmo.setIdEntrepriseLiée(Integer.parseInt(request.getParameter("idEntrepriseLiée")));
		nouvellefactureimmo.setNumeroDeCompte(request.getParameter("numeroDeCompte"));
		nouvellefactureimmo.setModePaiement(request.getParameter("modePaiement"));
		nouvellefactureimmo.setTauxAmort(Float.parseFloat(request.getParameter("tauxAmort")));
	

		Manager.getInstance().modifierFactureImmo(nouvellefactureimmo);
		
		response.sendRedirect("consultationFactureImmo");  
		
	}


}
