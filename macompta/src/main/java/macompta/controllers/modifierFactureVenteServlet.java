package macompta.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import macompta.beans.Entreprise;
import macompta.beans.FactureVente;
import macompta.metier.Manager;

/**
 * Servlet implementation class modifierFactureVenteServlet
 */
public class modifierFactureVenteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public modifierFactureVenteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idVente = Integer.parseInt(request.getParameter("idVente"));  
		  
		 FactureVente facturevente =Manager.getInstance().getFactureVenteById(idVente);  

        request.setAttribute("facturevente", facturevente);  

		
		List<FactureVente> factureventes = Manager.getInstance().listerFactureVente((Entreprise) request.getSession().getAttribute("entrepriseConnectee"));
		request.setAttribute("factureventes", factureventes);

		
		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/pages/modifierFactureVente.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		FactureVente nouvellefacturevente = new FactureVente(null, null, null, null, 0, 0, null, 0, null, null);
		Integer idVente = Integer.parseInt(request.getParameter("idVente"));
		nouvellefacturevente.setIdVente(idVente);  
		try {
			nouvellefacturevente.setDateVente(dateFormat.parse(request.getParameter("dateVente")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		nouvellefacturevente.setNomClient(request.getParameter("nomClient"));
		nouvellefacturevente.setLibelleFacture(request.getParameter("libelleFacture"));
		nouvellefacturevente.setMontantVHT(Float.parseFloat(request.getParameter("montantVHT")));
		nouvellefacturevente.setTvaV(Float.parseFloat(request.getParameter("tvaV")));
		try {
			nouvellefacturevente.setDateVGenerationEXCEL(dateFormat.parse(request.getParameter("dateVGenerationEXCEL")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		nouvellefacturevente.setIdEntrepriseLiée(Integer.parseInt(request.getParameter("idEntrepriseLiée")));
		nouvellefacturevente.setNumeroDeCompte(request.getParameter("numeroDeCompte"));
		nouvellefacturevente.setModePaiement(request.getParameter("modePaiement"));
	

		Manager.getInstance().modifierFactureVente(nouvellefacturevente);
		
		response.sendRedirect("consultationFactureVente");  
		
	}

}
