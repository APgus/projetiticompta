package macompta.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import macompta.beans.Compte;

public class CompteDao {
	
	
	
	public List<Compte> listerCompte() {
    	List<Compte> listecompte = new ArrayList<Compte>();
    	try {
    		Connection connection = DataSourceProvider.getDataSource()
    				.getConnection();

    		Statement stmt = connection.createStatement();
    		ResultSet results = stmt.executeQuery("SELECT * FROM Compte ");

    		while (results.next()) {
    			Compte compte = new Compte (
    					results.getString("numeroDeCompte"),
    					results.getString("libelleCompte"));
    			listecompte.add(compte);
    		}

    		// Fermer la connexion
    		results.close();
    		stmt.close();
    		connection.close();

    	} catch (SQLException e) {
    		e.printStackTrace();
    	}

    	return listecompte;
    }

}