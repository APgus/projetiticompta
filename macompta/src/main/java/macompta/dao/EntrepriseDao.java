package macompta.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import macompta.beans.Entreprise;

public class EntrepriseDao {

public boolean EntrepriseExiste(Entreprise entreprise){
	try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();
	
			// Utiliser la connexion
			PreparedStatement stmt = connection
					.prepareStatement("SELECT * FROM entreprise WHERE login = ? AND motDePasse = ? ");
			stmt.setString(1, entreprise.getLogin());
			stmt.setString(2, entreprise.getMotDePasse());
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				return true; 
			}
			stmt.close();
			connection.close();
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}




public Entreprise RecupererEntreprise(String login){
	Entreprise entrepriseconnectee = null;
	try {
		Connection connection = DataSourceProvider.getDataSource()
				.getConnection();

		// Utiliser la connexion
		PreparedStatement stmt = connection
				.prepareStatement("SELECT * FROM `entreprise` WHERE login = ?");
		stmt.setString(1, login);
		ResultSet result = stmt.executeQuery();
		if (result.next()) {
			entrepriseconnectee = new Entreprise(
				result.getInt("idEntreprise"),
				result.getString("login"),
				result.getString("motDePasse"),
				result.getString("nom"),
				result.getString("adresse"),
				result.getString("codePostal"),
				result.getString("ville"),
				result.getString("telephone"),
				result.getString("secteurDActivite"));
			
		}
		stmt.close();
		connection.close();

	} catch (SQLException e) {
		e.printStackTrace();
	}
	return entrepriseconnectee;
}

	
public List<Entreprise> listerEntreprise() {
	List<Entreprise> listeentreprise = new ArrayList<Entreprise>();
	try {
		Connection connection = DataSourceProvider.getDataSource()
				.getConnection();

		Statement stmt = connection.createStatement();
		ResultSet results = stmt.executeQuery("SELECT * FROM entreprise ");

		while (results.next()) {
			Entreprise entreprise = new Entreprise(
					results.getInt("idEntreprise"),
					results.getString("login"),
					results.getString("motDePasse"),
					results.getString("nom"),
					results.getString("adresse"),
					results.getString("codePostal"),
					results.getString("ville"),
					results.getString("telephone"),
					results.getString("secteurDActivite"));
			listeentreprise.add(entreprise);
		}

		// Fermer la connexion
		results.close();
		stmt.close();
		connection.close();

	} catch (SQLException e) {
		e.printStackTrace();
	}

	return listeentreprise;
}


	public void ajouterEntreprise(Entreprise entreprise){
		
		try {Connection connection = DataSourceProvider.getDataSource()
				.getConnection();
		
		PreparedStatement stmt = connection
				.prepareStatement("INSERT INTO `entreprise`(`login`,`motDePasse`,`nom`,`adresse`,`codePostal`,`ville`,`telephone`,`secteurDActivite`) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");

		stmt.setString(1, entreprise.getLogin());
		stmt.setString(2, entreprise.getMotDePasse());
		stmt.setString(3, entreprise.getNom());
		stmt.setString(4, entreprise.getAdresse());
		stmt.setString(5, entreprise.getCodePostal());
		stmt.setString(6, entreprise.getVille());
		stmt.setString(7, entreprise.getTelephone());
		stmt.setString(8, entreprise.getSecteurDActivite());
		stmt.executeUpdate();
		
		
		// Fermer la connexion
					stmt.close();
					connection.close();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}



}