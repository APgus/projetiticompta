package macompta.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import macompta.beans.Entreprise;
import macompta.beans.FactureAchat;

public class FactureAchatDao {

	public FactureAchat AfficherDerniereFactureAchats(Entreprise entreprise) {
		FactureAchat dernierefactureachat = null;
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM `facturedachat` WHERE `idEntrepriseLiée` = ? ORDER BY `idAchat` DESC LIMIT 1");
			
			stmt.setInt(1, entreprise.getIdEntreprise());
			ResultSet results = stmt.executeQuery();
			
			while (results.next()){
				dernierefactureachat = new FactureAchat(results.getInt("idAchat"),
						results.getDate("dateAchat"), results.getString("nomAFournisseur"),
						results.getString("libelleFacture"),
						results.getFloat("montantAHT"), results.getFloat("tvaA"), null ,
						results.getInt("idEntrepriseLiée"), results.getString("numeroDeCompte"), results.getString("modePaiement"));
				
			}

			// Fermer la connexion
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return dernierefactureachat;
	}
	
	public void ajouterFactureAchat(FactureAchat factureachat) {
		
		try {Connection connection = DataSourceProvider.getDataSource()
				.getConnection();
		
		PreparedStatement stmt = connection
				.prepareStatement("INSERT INTO `facturedachat`(`dateAchat`,`nomAFournisseur`,`libelleFacture`,`montantAHT`,`tvaA`,`idEntrepriseLiée`,`numeroDeCompte`,`modePaiement`) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");

		stmt.setDate(1, new Date(factureachat.getDateAchat().getTime()));
		stmt.setString(2, factureachat.getNomAFournisseur());
		stmt.setString(3, factureachat.getLibelleFacture());
		stmt.setFloat(4, factureachat.getMontantAHT());
		stmt.setFloat(5, factureachat.getTvaA());
		stmt.setInt(6, factureachat.getIdEntrepriseLiée());
		stmt.setString(7, factureachat.getNumeroDeCompte());
		stmt.setString(8, factureachat.getModePaiement());
		stmt.executeUpdate();
		
		
		// Fermer la connexion
					stmt.close();
					connection.close();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void supprimerDerniereFactureAchat(Entreprise entreprise) {
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			PreparedStatement stmt = connection.prepareStatement(" DELETE FROM `facturedachat` WHERE `idEntrepriseLiée` = ? ORDER BY `idAchat` DESC LIMIT 1");
			
			stmt.setInt(1, entreprise.getIdEntreprise());
			stmt.executeUpdate();
			
			stmt.close();
			connection.close();		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public List<FactureAchat> listerFactureAchatAGenerer(Entreprise entreprise) {
    	List<FactureAchat> listefactureachatagenerer = new ArrayList<FactureAchat>();
    	try {
    		
    		
    		Connection connection = DataSourceProvider.getDataSource().getConnection();

    		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM facturedachat WHERE `idEntrepriseLiée` = ? AND `dateGenerationExcel` IS NULL ");
    		
    		stmt.setInt(1, entreprise.getIdEntreprise());
    		ResultSet results = stmt.executeQuery();
    		
    		while (results.next()) {
    			FactureAchat factureachat = new FactureAchat (
    					results.getInt("idAchat"),
						results.getDate("dateAchat"),
						results.getString("nomAFournisseur"),
						results.getString("libelleFacture"),
						results.getFloat("montantAHT"), 
						results.getFloat("tvaA"), null ,
						results.getInt("idEntrepriseLiée"), 
						results.getString("numeroDeCompte"), 
						results.getString("modePaiement"));
    			listefactureachatagenerer.add(factureachat);
    		}

    		// Fermer la connexion
    		results.close();
    		stmt.close();
    		connection.close();

    	} catch (SQLException e) {
    		e.printStackTrace();
    	}

    	return listefactureachatagenerer;
    }

	
	
	public void validerGenerationExcel(FactureAchat facture,Entreprise entreprise) {  
		  
	    try {  

	    	Connection connection = DataSourceProvider.getDataSource().getConnection();

	    	PreparedStatement stmt = connection.prepareStatement("UPDATE facturedachat set dateGenerationExcel=? WHERE idAchat=? AND `idEntrepriseLiée` = ?");  

	    	stmt.setDate(1,new Date(facture.getDateAGenerationEXCEL().getTime()));  
	    	stmt.setInt(2, facture.getIdAchat());  
	    	stmt.setInt(3, entreprise.getIdEntreprise());  


	        stmt.executeUpdate();  

	    } catch (SQLException e) {  

	        e.printStackTrace();  

	    }  

	}

	
	public List<FactureAchat> listerFactureAchat(Entreprise entreprise) {
    	List<FactureAchat> listefactureachat = new ArrayList<FactureAchat>();
    	try {
    		
    		
    		Connection connection = DataSourceProvider.getDataSource().getConnection();

    		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM facturedachat WHERE `idEntrepriseLiée` = ? ");
    		
    		stmt.setInt(1, entreprise.getIdEntreprise());
    		ResultSet results = stmt.executeQuery();
    		
    		while (results.next()) {
    			FactureAchat factureachat = new FactureAchat (
    					results.getInt("idAchat"),
						results.getDate("dateAchat"),
						results.getString("nomAFournisseur"),
						results.getString("libelleFacture"),
						results.getFloat("montantAHT"), 
						results.getFloat("tvaA"), 
						results.getDate("dateGenerationExcel") ,
						results.getInt("idEntrepriseLiée"), 
						results.getString("numeroDeCompte"), 
						results.getString("modePaiement"));
    			listefactureachat.add(factureachat);
    		}

    		// Fermer la connexion
    		results.close();
    		stmt.close();
    		connection.close();

    	} catch (SQLException e) {
    		e.printStackTrace();
    	}

    	return listefactureachat;
    }

	
	public void supprimerFacture (FactureAchat factureachat) {
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			PreparedStatement stmt = connection.prepareStatement("DELETE FROM facturedachat WHERE idAchat=?");
			stmt.setInt(1, factureachat.getIdAchat());
			stmt.executeUpdate();
			
			stmt.close();
			connection.close();		
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public void modifierFactureAchat(FactureAchat factureachat) {  
		  
	    try {  

	    	Connection connection = DataSourceProvider.getDataSource().getConnection();

	    	PreparedStatement stmt = connection.prepareStatement("UPDATE facturedachat set dateAchat=?, nomAFournisseur=?, libelleFacture=?, montantAHT=?,tvaA=?, numeroDeCompte=?,modePaiement=? WHERE idAchat=?");  

	    	stmt.setDate(1,new Date(factureachat.getDateAchat().getTime()));
	    	stmt.setString(2, factureachat.getNomAFournisseur());  
	    	stmt.setString(3, factureachat.getLibelleFacture());
	    	stmt.setFloat(4, factureachat.getMontantAHT());
	    	stmt.setFloat(5, factureachat.getTvaA());
	    	stmt.setString(6, factureachat.getNumeroDeCompte());
	    	stmt.setString(7, factureachat.getModePaiement());
	    	stmt.setInt(8, factureachat.getIdAchat());  
	    	;  

	        stmt.executeUpdate();  

	    } catch (SQLException e) {  

	        e.printStackTrace();  

	    }  

	}
	
	
	public FactureAchat getFactureAchatById(int idAchat) {  
  	  
		FactureAchat factureachat = new FactureAchat(idAchat, null, null, null, 0, 0, null, 0, null, null);  
  
        try { 
        	
        	Connection connection = DataSourceProvider.getDataSource().getConnection();
        
  
            PreparedStatement preparedStatement = connection.  
  
                    prepareStatement("select * from facturedachat where idAchat=?");  
  
            preparedStatement.setInt(1, idAchat);  
  
            ResultSet rs = preparedStatement.executeQuery();  
 
            factureachat.setIdAchat(rs.getInt("idAchat"));  
            factureachat.setDateAchat(rs.getDate("dateAchat"));  
            factureachat.setNomFournisseur(rs.getString("nomAFournisseur"));
            factureachat.setLibelleFacture(rs.getString("libelleFacture"));  
            factureachat.setMontantAHT(rs.getFloat("montantAHT"));  
            factureachat.setTvaA(rs.getFloat("tvaA"));  
            factureachat.setDateAGenerationEXCEL(rs.getDate("dateGenerationExcel"));  
            factureachat.setNumeroDeCompte(rs.getString("numeroDeCompte"));  
            factureachat.setModePaiement(rs.getString("modePaiement"));  
            

        } catch (SQLException e) {  
  
            e.printStackTrace();  
  
        }  
  
        return factureachat;  
  
    }  
	
	
	public void enleverDateGenerationExcel(FactureAchat factureachat) {  
		  
	    try {  

	    	Connection connection = DataSourceProvider.getDataSource().getConnection();

	    	PreparedStatement stmt = connection.prepareStatement("UPDATE facturedachat set dateGenerationExcel = NULL WHERE idAchat=?");  

	    	stmt.setInt(1, factureachat.getIdAchat());  
	    	;  

	        stmt.executeUpdate();  

	    } catch (SQLException e) {  

	        e.printStackTrace();  

	    }  

	}
	
	
}
