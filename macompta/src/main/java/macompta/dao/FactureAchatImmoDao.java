package macompta.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import macompta.beans.Entreprise;
import macompta.beans.FactureAchatImmobilisation;

public class FactureAchatImmoDao {

	public FactureAchatImmobilisation AfficherDerniereFactureAchatsImmo(Entreprise entreprise) {
		FactureAchatImmobilisation dernierefactureachatimmo = null;
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM `facturedachatimmobilisation` WHERE `idEntrepriseLiée` = ? ORDER BY `idImmo` DESC LIMIT 1");

			stmt.setInt(1, entreprise.getIdEntreprise());
			ResultSet results = stmt.executeQuery();
			
			while (results.next()){
				dernierefactureachatimmo = new FactureAchatImmobilisation(results.getInt("idImmo"),
						results.getDate("dateImmo"), results.getString("nomIFournisseur"),results.getString("libelleFacture"),
						results.getFloat("montantIHT"), results.getFloat("tvaI"), null ,
						results.getInt("idEntrepriseLiée"), results.getString("numeroDeCompte"),results.getFloat("tauxAmort"), results.getString("modePaiement"));
				
			}

			// Fermer la connexion
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return dernierefactureachatimmo;
	}

	public void ajouterFactureAchatImmobilisation(FactureAchatImmobilisation facturedachatimmobilisation) {
			try {Connection connection = DataSourceProvider.getDataSource()
						.getConnection();
			
			PreparedStatement stmt = connection
					.prepareStatement("INSERT INTO `facturedachatimmobilisation`(`dateImmo`,`nomIFournisseur`,`libelleFacture`,`montantIHT`,`tvaI`,`idEntrepriseLiée`,`numeroDeCompte`,`tauxAmort`,`modePaiement`) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");

			stmt.setDate(1, new Date(facturedachatimmobilisation.getDateImmo().getTime()));
			stmt.setString(2, facturedachatimmobilisation.getNomIFournisseur());
			stmt.setString(3, facturedachatimmobilisation.getLibelleFacture());
			stmt.setFloat(4, facturedachatimmobilisation.getMontantIHT());
			stmt.setFloat(5, facturedachatimmobilisation.getTvaI());
			stmt.setInt(6, facturedachatimmobilisation.getIdEntrepriseLiée());
			stmt.setString(7, facturedachatimmobilisation.getNumeroDeCompte());
			stmt.setFloat(8, facturedachatimmobilisation.getTauxAmort());
			stmt.setString(9, facturedachatimmobilisation.getModePaiement());
			stmt.executeUpdate();
			
			
			// Fermer la connexion
						stmt.close();
						connection.close();
			
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	
	
	
	
	
	public void supprimerDerniereFactureAchatImmo(Entreprise entreprise) {
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			PreparedStatement stmt = connection.prepareStatement(" DELETE FROM `facturedachatimmobilisation` WHERE `idEntrepriseLiée` = ? ORDER BY `idImmo` DESC LIMIT 1");
			
			stmt.setInt(1, entreprise.getIdEntreprise());
			stmt.executeUpdate();
			
			stmt.close();
			connection.close();		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	public List<FactureAchatImmobilisation> listerFactureAchatImmoAGenerer(Entreprise entreprise) {
    	List<FactureAchatImmobilisation> listefactureachatimmoagenerer = new ArrayList<FactureAchatImmobilisation>();
    	try {
    		
    		
    		Connection connection = DataSourceProvider.getDataSource().getConnection();

    		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM facturedachatimmobilisation WHERE `idEntrepriseLiée` = ? AND `dateGenerationExcel` IS NULL ");
    		
    		stmt.setInt(1, entreprise.getIdEntreprise());
    		ResultSet results = stmt.executeQuery();
    		
    		while (results.next()) {
    			FactureAchatImmobilisation factureachatimmo = new FactureAchatImmobilisation (
    					results.getInt("idImmo"),
						results.getDate("dateImmo"),
						results.getString("nomIFournisseur"),
						results.getString("libelleFacture"),
						results.getFloat("montantIHT"), 
						results.getFloat("tvaI"), null ,
						results.getInt("idEntrepriseLiée"), 
						results.getString("numeroDeCompte"),
						results.getFloat("tauxAmort"),
						results.getString("modePaiement"));
    			listefactureachatimmoagenerer.add(factureachatimmo);
    		}

    		// Fermer la connexion
    		results.close();
    		stmt.close();
    		connection.close();

    	} catch (SQLException e) {
    		e.printStackTrace();
    	}

    	return listefactureachatimmoagenerer;
    }

	
	
	public void validerGenerationExcel(FactureAchatImmobilisation facture,Entreprise entreprise) {  
		  
	    try {  

	    	Connection connection = DataSourceProvider.getDataSource().getConnection();

	    	PreparedStatement stmt = connection.prepareStatement("UPDATE facturedachatimmobilisation set dateGenerationExcel=? WHERE idImmo=? AND `idEntrepriseLiée` = ?");  

	    	stmt.setDate(1,new Date(facture.getDateIGenerationEXCEL().getTime()));  
	    	stmt.setInt(2, facture.getIdImmo());  
	    	stmt.setInt(3, entreprise.getIdEntreprise());  


	        stmt.executeUpdate();  

	    } catch (SQLException e) {  

	        e.printStackTrace();  

	    }  

	}

	public List<FactureAchatImmobilisation> listerFactureImmo(Entreprise entreprise) {
    	List<FactureAchatImmobilisation> listefactureimmo = new ArrayList<FactureAchatImmobilisation>();
    	try {
    		
    		
    		Connection connection = DataSourceProvider.getDataSource().getConnection();

    		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM facturedachatimmobilisation WHERE `idEntrepriseLiée` = ? ");
    		
    		stmt.setInt(1, entreprise.getIdEntreprise());
    		ResultSet results = stmt.executeQuery();
    		
    		while (results.next()) {
    			FactureAchatImmobilisation factureimmo = new FactureAchatImmobilisation (
    					results.getInt("idImmo"),
						results.getDate("dateImmo"),
						results.getString("nomIFournisseur"),
						results.getString("libelleFacture"),
						results.getFloat("montantIHT"), 
						results.getFloat("tvaI"), 
						results.getDate("dateGenerationExcel") ,
						results.getInt("idEntrepriseLiée"), 
						results.getString("numeroDeCompte"), 
						results.getFloat("tauxAmort"), 
						results.getString("modePaiement"));
    			listefactureimmo.add(factureimmo);
    		}

    		// Fermer la connexion
    		results.close();
    		stmt.close();
    		connection.close();

    	} catch (SQLException e) {
    		e.printStackTrace();
    	}

    	return listefactureimmo;
    }

	
	public void supprimerFacture (FactureAchatImmobilisation factureimmo) {
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			PreparedStatement stmt = connection.prepareStatement("DELETE FROM facturedachatimmobilisation WHERE idImmo=?");
			stmt.setInt(1, factureimmo.getIdImmo());
			stmt.executeUpdate();
			
			stmt.close();
			connection.close();		
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public void modifierFactureImmo(FactureAchatImmobilisation factureimmo) {  
		  
	    try {  

	    	Connection connection = DataSourceProvider.getDataSource().getConnection();

	    	PreparedStatement stmt = connection.prepareStatement("UPDATE facturedachatimmobilisation set dateImmo=?, nomIFournisseur=?, libelleFacture=?, montantIHT=?,tvaI=?, numeroDeCompte=?,tauxAmort=?,modePaiement=? WHERE idImmo=?");  

	    	stmt.setDate(1,new Date(factureimmo.getDateImmo().getTime()));
	    	stmt.setString(2, factureimmo.getNomIFournisseur());  
	    	stmt.setString(3, factureimmo.getLibelleFacture());
	    	stmt.setFloat(4, factureimmo.getMontantIHT());
	    	stmt.setFloat(5, factureimmo.getTvaI());
	    	stmt.setString(6, factureimmo.getNumeroDeCompte());
	     	stmt.setFloat(7, factureimmo.getTauxAmort());
	    	stmt.setString(8, factureimmo.getModePaiement());
	    	stmt.setInt(9, factureimmo.getIdImmo());  
	    	;  

	        stmt.executeUpdate();  

	    } catch (SQLException e) {  

	        e.printStackTrace();  

	    }  

	}
	
	
	public FactureAchatImmobilisation getFactureImmoById(int idImmo) {  
  	  
		FactureAchatImmobilisation factureimmo = new FactureAchatImmobilisation(idImmo, null, null, null, 0, 0, null, 0, null, null, null);  
  
        try { 
        	
        	Connection connection = DataSourceProvider.getDataSource().getConnection();
        
  
            PreparedStatement preparedStatement = connection.  
  
                    prepareStatement("select * from facturedachatimmobilisation where idImmo=?");  
  
            preparedStatement.setInt(1, idImmo);  
  
            ResultSet rs = preparedStatement.executeQuery();  
 
            factureimmo.setIdImmo(rs.getInt("idImmo"));  
            factureimmo.setDateImmo(rs.getDate("dateImmo"));  
            factureimmo.setNomIFournisseur(rs.getString("nomiFournisseur"));
            factureimmo.setLibelleFacture(rs.getString("libelleFacture"));  
            factureimmo.setMontantIHT(rs.getFloat("montantIHT"));  
            factureimmo.setTvaI(rs.getFloat("tvaI"));  
            factureimmo.setDateIGenerationEXCEL(rs.getDate("dateGenerationExcel"));  
            factureimmo.setNumeroDeCompte(rs.getString("numeroDeCompte"));  
            factureimmo.setModePaiement(rs.getString("modePaiement"));  
            

        } catch (SQLException e) {  
  
            e.printStackTrace();  
  
        }  
  
        return factureimmo;  
  
    }  
	
	
	public void enleverDateGenerationExcel(FactureAchatImmobilisation factureimmo) {  
		  
	    try {  

	    	Connection connection = DataSourceProvider.getDataSource().getConnection();

	    	PreparedStatement stmt = connection.prepareStatement("UPDATE facturedachatimmobilisation set dateGenerationExcel = NULL WHERE idImmo=?");  

	    	stmt.setInt(1, factureimmo.getIdImmo());  
	    	;  

	        stmt.executeUpdate();  

	    } catch (SQLException e) {  

	        e.printStackTrace();  

	    }  

	}
	
	
}
