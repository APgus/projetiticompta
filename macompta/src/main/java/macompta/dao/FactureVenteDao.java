package macompta.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import macompta.beans.Entreprise;
import macompta.beans.FactureVente;

public class FactureVenteDao {

	public FactureVente AfficherDerniereFactureVentes(Entreprise entreprise) {
		FactureVente dernierefacturevente = null;
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM `facturedevente` WHERE `idEntrepriseLiée` = ? ORDER BY `idVente` DESC LIMIT 1");
			
			stmt.setInt(1, entreprise.getIdEntreprise());
			ResultSet results = stmt.executeQuery();
			
			while (results.next()){
				dernierefacturevente = new FactureVente(results.getInt("idVente"),
						results.getDate("dateVente"), results.getString("nomClient"),
						results.getString("libelleFacture"),
						results.getFloat("montantVHT"), results.getFloat("tvaV"), null ,
						results.getInt("idEntrepriseLiée"), results.getString("numeroDeCompte"), results.getString("modePaiement"));
				
			}

			// Fermer la connexion
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return dernierefacturevente;
	}

	public void ajouterFactureVente(FactureVente facturevente) {
		
		try {Connection connection = DataSourceProvider.getDataSource()
				.getConnection();
	
	PreparedStatement stmt = connection
			.prepareStatement("INSERT INTO `facturedevente`(`dateVente`,`nomClient`,`libelleFacture`,`montantVHT`,`tvaV`,`idEntrepriseLiée`,`numeroDeCompte`,`modePaiement`) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");

	stmt.setDate(1, new Date(facturevente.getDateVente().getTime()));
	stmt.setString(2, facturevente.getNomClient());
	stmt.setString(3, facturevente.getLibelleFacture());
	stmt.setFloat(4, facturevente.getMontantVHT());
	stmt.setFloat(5, facturevente.getTvaV());
	stmt.setInt(6, facturevente.getIdEntrepriseLiée());
	stmt.setString(7, facturevente.getNumeroDeCompte());
	stmt.setString(8, facturevente.getModePaiement());
	stmt.executeUpdate();
	
	
	// Fermer la connexion
				stmt.close();
				connection.close();
	
		} catch (SQLException e) {
		e.printStackTrace();
		}
	}
	
	
	public void supprimerDerniereFactureVente(Entreprise entreprise) {
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			PreparedStatement stmt = connection.prepareStatement(" DELETE FROM `facturedevente` WHERE `idEntrepriseLiée` = ? ORDER BY `idVente` DESC LIMIT 1");
			
			stmt.setInt(1, entreprise.getIdEntreprise());
			stmt.executeUpdate();
			
			stmt.close();
			connection.close();		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public List<FactureVente> listerFactureVenteAGenerer(Entreprise entreprise) {
    	List<FactureVente> listefactureventeagenerer = new ArrayList<FactureVente>();
    	try {
    		
    		
    		Connection connection = DataSourceProvider.getDataSource().getConnection();

    		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM facturedevente WHERE `idEntrepriseLiée` = ? AND `dateGenerationExcel` IS NULL ");
    		
    		stmt.setInt(1, entreprise.getIdEntreprise());
    		ResultSet results = stmt.executeQuery();
    		
    		while (results.next()) {
    			FactureVente facturevente = new FactureVente (
    					results.getInt("idVente"),
						results.getDate("dateVente"),
						results.getString("nomClient"),
						results.getString("libelleFacture"),
						results.getFloat("montantVHT"), 
						results.getFloat("tvaV"), null ,
						results.getInt("idEntrepriseLiée"), 
						results.getString("numeroDeCompte"), 
						results.getString("modePaiement"));
    			listefactureventeagenerer.add(facturevente);
    		}

    		// Fermer la connexion
    		results.close();
    		stmt.close();
    		connection.close();

    	} catch (SQLException e) {
    		e.printStackTrace();
    	}

    	return listefactureventeagenerer;
    }

	
	
	public void validerGenerationExcel(FactureVente facture,Entreprise entreprise) {  
		  
	    try {  

	    	Connection connection = DataSourceProvider.getDataSource().getConnection();

	    	PreparedStatement stmt = connection.prepareStatement("UPDATE facturedevente set dateGenerationExcel=? WHERE idVente=? AND `idEntrepriseLiée` = ?");  

	    	stmt.setDate(1,new Date(facture.getDateVGenerationEXCEL().getTime()));  
	    	stmt.setInt(2, facture.getIdVente());  
	    	stmt.setInt(3, entreprise.getIdEntreprise());  


	        stmt.executeUpdate();  

	    } catch (SQLException e) {  

	        e.printStackTrace();  

	    }  

	}

	public List<FactureVente> listerFactureVente(Entreprise entreprise) {
    	List<FactureVente> listefacturevente = new ArrayList<FactureVente>();
    	try {
    		
    		
    		Connection connection = DataSourceProvider.getDataSource().getConnection();

    		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM facturedevente WHERE `idEntrepriseLiée` = ? ");
    		
    		stmt.setInt(1, entreprise.getIdEntreprise());
    		ResultSet results = stmt.executeQuery();
    		
    		while (results.next()) {
    			FactureVente facturevente = new FactureVente (
    					results.getInt("idVente"),
						results.getDate("dateVente"),
						results.getString("nomClient"),
						results.getString("libelleFacture"),
						results.getFloat("montantVHT"), 
						results.getFloat("tvaV"), 
						results.getDate("dateGenerationExcel") ,
						results.getInt("idEntrepriseLiée"), 
						results.getString("numeroDeCompte"), 
						results.getString("modePaiement"));
    			listefacturevente.add(facturevente);
    		}

    		// Fermer la connexion
    		results.close();
    		stmt.close();
    		connection.close();

    	} catch (SQLException e) {
    		e.printStackTrace();
    	}

    	return listefacturevente;
    }

	
	public void supprimerFacture (FactureVente facturevente) {
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			PreparedStatement stmt = connection.prepareStatement("DELETE FROM facturedevente WHERE idVente=?");
			stmt.setInt(1, facturevente.getIdVente());
			stmt.executeUpdate();
			
			stmt.close();
			connection.close();		
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public void modifierFactureVente(FactureVente facturevente) {  
		  
	    try {  

	    	Connection connection = DataSourceProvider.getDataSource().getConnection();

	    	PreparedStatement stmt = connection.prepareStatement("UPDATE facturedevente set dateVente=?, nomClient=?, libelleFacture=?, montantVHT=?,tvaV=?, numeroDeCompte=?,modePaiement=? WHERE idVente=?");  

	    	stmt.setDate(1,new Date(facturevente.getDateVente().getTime()));
	    	stmt.setString(2, facturevente.getNomClient());  
	    	stmt.setString(3, facturevente.getLibelleFacture());
	    	stmt.setFloat(4, facturevente.getMontantVHT());
	    	stmt.setFloat(5, facturevente.getTvaV());
	    	stmt.setString(6, facturevente.getNumeroDeCompte());
	    	stmt.setString(7, facturevente.getModePaiement());
	    	stmt.setInt(8, facturevente.getIdVente());  
	    	;  

	        stmt.executeUpdate();  

	    } catch (SQLException e) {  

	        e.printStackTrace();  

	    }  

	}
	
	
	public FactureVente getFactureVenteById(int idVente) {  
  	  
		FactureVente facturevente = new FactureVente(idVente, null, null, null, 0, 0, null, 0, null, null);  
  
        try { 
        	
        	Connection connection = DataSourceProvider.getDataSource().getConnection();
        
  
            PreparedStatement preparedStatement = connection.  
  
                    prepareStatement("select * from facturedevente where idVente=?");  
  
            preparedStatement.setInt(1, idVente);  
  
            ResultSet rs = preparedStatement.executeQuery();  
 
            facturevente.setIdVente(rs.getInt("idVente"));  
            facturevente.setDateVente(rs.getDate("dateVente"));  
            facturevente.setNomClient(rs.getString("nomClient"));
            facturevente.setLibelleFacture(rs.getString("libelleFacture"));  
            facturevente.setMontantVHT(rs.getFloat("montantVHT"));  
            facturevente.setTvaV(rs.getFloat("tvaV"));  
            facturevente.setDateVGenerationEXCEL(rs.getDate("dateGenerationExcel"));  
            facturevente.setNumeroDeCompte(rs.getString("numeroDeCompte"));  
            facturevente.setModePaiement(rs.getString("modePaiement"));  
            

        } catch (SQLException e) {  
  
            e.printStackTrace();  
  
        }  
  
        return facturevente;  
  
    }  
	
	
	public void enleverDateGenerationExcel(FactureVente facturevente) {  
		  
	    try {  

	    	Connection connection = DataSourceProvider.getDataSource().getConnection();

	    	PreparedStatement stmt = connection.prepareStatement("UPDATE facturedevente set dateGenerationExcel = NULL WHERE idVente=?");  

	    	stmt.setInt(1, facturevente.getIdVente());  
	    	;  

	        stmt.executeUpdate();  

	    } catch (SQLException e) {  

	        e.printStackTrace();  

	    }  

	}
	
	
}
