package macompta.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import macompta.beans.Entreprise;

public class RestrictionFilter implements Filter {
	
	
	public void init(FilterConfig filterConfig) throws ServletException {
	}
	
	
    public void doFilter(ServletRequest request, ServletResponse response,    FilterChain chain) throws IOException, ServletException {
       
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        
        
        String path = req.getRequestURI().substring( req.getContextPath().length() );
        if ( path.startsWith( "/css") || path.startsWith("/images") || path.startsWith("/js") || path.matches("../images")) {
        	          
            chain.doFilter( request, response );
            return;
        }
        
        
        if(!req.getServletPath().equals("/connexion") &&!req.getServletPath().equals("/aide")) {
        	testConnexionEntreprise(req,resp,chain);
        }else{
            chain.doFilter(request, response);
        }
        
        
       
    }
   
   
    private void testConnexionEntreprise(HttpServletRequest req,HttpServletResponse resp,FilterChain chain) throws IOException, ServletException {
        Entreprise entrepriseConnectee = RecupererEntreprise(req);
        	if(entrepriseConnectee==null){
            resp.sendRedirect(req.getServletContext().getContextPath()+"/connexion");
        }else{
            chain.doFilter(req, resp);
        }
    }
    
    
    private Entreprise RecupererEntreprise(HttpServletRequest req) {
        return (Entreprise) req.getSession().getAttribute("entrepriseConnectee");
    }
    
    
    
    public void destroy() {
    }
}