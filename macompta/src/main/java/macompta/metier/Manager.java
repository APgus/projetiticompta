package macompta.metier;

import java.util.List;

import macompta.beans.Compte;
import macompta.beans.Entreprise;
import macompta.beans.FactureAchat;
import macompta.beans.FactureAchatImmobilisation;
import macompta.beans.FactureVente;
import macompta.dao.CompteDao;
import macompta.dao.EntrepriseDao;
import macompta.dao.FactureAchatDao;
import macompta.dao.FactureAchatImmoDao;
import macompta.dao.FactureVenteDao;



public class Manager {

	private static Manager instance;
	
	private FactureAchatDao factureachatDao;
	private FactureAchatImmoDao factureachatimmoDao;
	private FactureVenteDao factureventeDao;
	private EntrepriseDao entrepriseDao;
	private CompteDao comptedao;
	
	public static Manager getInstance() {
		if(instance == null) {
			instance = new Manager();
		}
		return instance;
	}
	

	private Manager() {
		factureachatDao = new FactureAchatDao();
		factureachatimmoDao = new FactureAchatImmoDao();
		factureventeDao = new FactureVenteDao();
		entrepriseDao = new EntrepriseDao();
		comptedao = new CompteDao();
	}

/*----------------------------------------------Entreprise-------------------------------------------------------------*/	
	
	public boolean EntrepriseExiste(Entreprise entreprise){
		return entrepriseDao.EntrepriseExiste(entreprise);
	}
	
	
	public Entreprise RecupererEntreprise(String login){
		return entrepriseDao.RecupererEntreprise(login);
		
	}
	
	public List<Entreprise> listerEntreprise() {
		List<Entreprise> listeentreprise = entrepriseDao.listerEntreprise();
		return listeentreprise;
	}
	

	public void ajouterEntreprise(Entreprise entreprise) {
		entrepriseDao.ajouterEntreprise(entreprise);
	}
	
/*----------------------------------------------Factures d'achats-----------------------------------------------------*/
	
	public FactureAchat AfficherDerniereFactureAchats(Entreprise entreprise) {
		FactureAchat dernierefactureachat = factureachatDao.AfficherDerniereFactureAchats(entreprise);
		return dernierefactureachat;
	}
	
	
	public void ajouterFactureAchat(FactureAchat factureachat) {
		factureachatDao.ajouterFactureAchat(factureachat);
	}
	
	public void supprimerDerniereFactureAchat(Entreprise entreprise) {
		factureachatDao.supprimerDerniereFactureAchat(entreprise);
	}
	
	
	public List<FactureAchat> listefactureachatagenerer(Entreprise entreprise) {
		List<FactureAchat> listefactureachatagenerer = factureachatDao.listerFactureAchatAGenerer(entreprise);
		return listefactureachatagenerer;
	}

	public void validerGenerationExcel(FactureAchat factureachat, Entreprise entreprise) {
		factureachatDao.validerGenerationExcel(factureachat, entreprise);
	}
	
	public List<FactureAchat> listerFactureAchat(Entreprise entreprise) {
		List<FactureAchat> listefactureachat = factureachatDao.listerFactureAchat(entreprise);
		return listefactureachat;
	}
	
	public void supprimerFacture(FactureAchat factureachat) {
		factureachatDao.supprimerFacture(factureachat);
	}
	
	public void modifierFactureAchat(FactureAchat factureachat) {
		factureachatDao.modifierFactureAchat(factureachat);
	}
	
	
	public FactureAchat getFactureAchatById(int idAchat) {
		FactureAchat factureachat = factureachatDao.getFactureAchatById(idAchat);
		return factureachat;
	}
	
	public void enleverDateGenerationExcel(FactureAchat factureachat) {
		factureachatDao.enleverDateGenerationExcel(factureachat);
	}
	
/*------------------------------------------Factures d'achats d'immo---------------------------------------------------*/
	
	
	public FactureAchatImmobilisation AfficherDerniereFactureAchatsImmo(Entreprise entreprise) {
		FactureAchatImmobilisation dernierefactureachatimmo = factureachatimmoDao.AfficherDerniereFactureAchatsImmo(entreprise);
		return dernierefactureachatimmo;
	}
	
	public void ajouterFactureAchatImmobilisation(FactureAchatImmobilisation factureachatimmobilisation) {
		factureachatimmoDao.ajouterFactureAchatImmobilisation(factureachatimmobilisation);
	}

	
	public void supprimerDerniereFactureAchatImmo(Entreprise entreprise) {
		factureachatimmoDao.supprimerDerniereFactureAchatImmo(entreprise);
		
	}

	public List<FactureAchatImmobilisation> listerFactureAchatImmoAGenerer(Entreprise entreprise) {
		List<FactureAchatImmobilisation> listefactureachatimmoagenerer = factureachatimmoDao.listerFactureAchatImmoAGenerer(entreprise);
		return listefactureachatimmoagenerer;
	}

	public void validerGenerationExcel(FactureAchatImmobilisation factureachatimmo, Entreprise entreprise) {
		factureachatimmoDao.validerGenerationExcel(factureachatimmo, entreprise);
	}
	
	public List<FactureAchatImmobilisation> listerFactureImmo(Entreprise entreprise) {
		List<FactureAchatImmobilisation> listefacturevente = factureachatimmoDao.listerFactureImmo(entreprise);
		return listefacturevente;
	}
	
	public void supprimerFacture(FactureAchatImmobilisation factureimmo) {
		factureachatimmoDao.supprimerFacture(factureimmo);
	}
	
	public void modifierFactureImmo(FactureAchatImmobilisation factureimmo) {
		factureachatimmoDao.modifierFactureImmo(factureimmo);
	}
	
	
	public FactureAchatImmobilisation getFactureImmoById(int idImmo) {
		FactureAchatImmobilisation factureimmo = factureachatimmoDao.getFactureImmoById(idImmo);
		return factureimmo;
	}
	
	public void enleverDateGenerationExcel(FactureAchatImmobilisation factureimmo) {
		factureachatimmoDao.enleverDateGenerationExcel(factureimmo);
	}
/*-------------------------------------------Factures de ventes----------------------------------------------------------*/
	

	public FactureVente AfficherDerniereFactureVentes(Entreprise entreprise) {
		FactureVente dernierefacturevente = factureventeDao.AfficherDerniereFactureVentes(entreprise);
		return dernierefacturevente;

	}


	public void ajouterFactureVente(FactureVente facturevente) {
		factureventeDao.ajouterFactureVente(facturevente);
		
	}

	public void supprimerDerniereFactureVente(Entreprise entreprise) {
		factureventeDao.supprimerDerniereFactureVente(entreprise);
		
	}
	
	public List<FactureVente> listerFactureVenteAGenerer(Entreprise entreprise) {
		List<FactureVente> listefactureachatagenerer = factureventeDao.listerFactureVenteAGenerer(entreprise);
		return listefactureachatagenerer;
	}

	public void validerGenerationExcel(FactureVente facturevente, Entreprise entreprise) {
		factureventeDao.validerGenerationExcel(facturevente, entreprise);
	}
	
	public List<FactureVente> listerFactureVente(Entreprise entreprise) {
		List<FactureVente> listefacturevente = factureventeDao.listerFactureVente(entreprise);
		return listefacturevente;
	}
	
	public void supprimerFacture(FactureVente facturevente) {
		factureventeDao.supprimerFacture(facturevente);
	}
	
	public void modifierFactureVente(FactureVente facturevente) {
		factureventeDao.modifierFactureVente(facturevente);
	}
	
	
	public FactureVente getFactureVenteById(int idVente) {
		FactureVente facturevente = factureventeDao.getFactureVenteById(idVente);
		return facturevente;
	}
	
	public void enleverDateGenerationExcel(FactureVente facturevente) {
		factureventeDao.enleverDateGenerationExcel(facturevente);
	}
/*------------------------------------------------Comtpe----------------------------------------------------------------*/	

	
	public List<Compte> listerCompte() {
		List<Compte> listecompte = comptedao.listerCompte();
		return listecompte;
	}
	
}