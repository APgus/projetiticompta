-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 12 Mai 2014 à 22:52
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `macompta`
--

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE IF NOT EXISTS `compte` (
  `NumeroDeCompte` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  `libelleCompte` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  PRIMARY KEY (`NumeroDeCompte`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Contenu de la table `compte`
--

INSERT INTO `compte` (`NumeroDeCompte`, `libelleCompte`) VALUES
('2000', 'immobilisations incorporelles'),
('2100', 'immobilisations corporelles'),
('2200', 'immobilisations mises en concession'),
('2300', 'immobilisations en cours'),
('6010', 'achats stockes - matieres premieres et fournitures'),
('6020', 'achats stockes - autres approvisionnements'),
('6040', 'achats d etudes et prestations de services'),
('6050', 'achats de materiel equipement et travaux'),
('6061', 'fournitures non stockables (eau, energies)'),
('6062', 'fournitures d entretiens et de petit equipement'),
('6063', 'fournitures administratives'),
('6064', 'autres matieres et fournitures'),
('6070', 'achats de marchandises'),
('6900', 'participation des salaries impots sur les societes'),
('6800', 'dotations aux amortissements aux depreciations et aux provisions'),
('6700', 'charges exceptionnelles'),
('6600', 'charges financieres'),
('6500', 'autres charges de gestion courante'),
('6400', 'charges de personnel'),
('6300', 'impots taxes et versements assimiles'),
('6200', 'autres services extérieurs'),
('6100', 'services extérieurs'),
('7010', 'ventes de produits finis'),
('7020', 'ventes de produits intermediaires'),
('7030', 'ventes de produits residuels'),
('7040', 'travaux'),
('7050', 'etudes'),
('7060', 'prestations de services'),
('7070', 'ventes de marchandises'),
('7080', 'produits des activites annexes'),
('7090', 'rabais remises et ristournes accordees'),
('7100', 'production stockee'),
('7200', 'production immobilisee'),
('7400', 'subventions d exploitations'),
('7500', 'autres produits de gestion courante'),
('760', 'produits financiers'),
('770', 'produits exceptionnels'),
('780', 'reprise sur amortissements et provisions'),
('790', 'transfert de charges');

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE IF NOT EXISTS `entreprise` (
  `idEntreprise` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  `motDePasse` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  `nom` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  `adresse` varchar(500) COLLATE utf8_unicode_520_ci NOT NULL,
  `codePostal` varchar(5) COLLATE utf8_unicode_520_ci NOT NULL,
  `ville` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  `telephone` varchar(10) COLLATE utf8_unicode_520_ci NOT NULL,
  `secteurDActivite` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  PRIMARY KEY (`idEntreprise`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `entreprise`
--

INSERT INTO `entreprise` (`idEntreprise`, `login`, `motDePasse`, `nom`, `adresse`, `codePostal`, `ville`, `telephone`, `secteurDActivite`) VALUES
(1, 'pierre.sulpice@sulpiceconseil.fr', 'pierre', 'Pierre Sulpice Expertise', '50 Boulevard de la courtille', '28000', 'Chartres', '0609175804', 'comptabilite'),
(2, 'augustin.picot@hei.fr', 'augustin', 'Picot Peche et Chasse', '1 rue du Moulinet', '62032', 'Hinges', '0622029446', 'magasin de peche'),
(3, 'francois-eric.sulpice@hei.fr', 'francois', 'La Mie de Pain', '12 rue Petrin', '59000', 'Lille', '0624805843', 'boulangerie');

-- --------------------------------------------------------

--
-- Structure de la table `facturedachat`
--

CREATE TABLE IF NOT EXISTS `facturedachat` (
  `idAchat` int(50) NOT NULL AUTO_INCREMENT,
  `dateAchat` date DEFAULT NULL,
  `nomAFournisseur` varchar(100) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `libelleFacture` varchar(100) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `montantAHT` double NOT NULL,
  `tvaA` double NOT NULL,
  `dateGenerationExcel` date DEFAULT NULL,
  `idEntrepriseLiée` int(11) NOT NULL,
  `numeroDeCompte` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  `modePaiement` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  PRIMARY KEY (`idAchat`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `facturedachat`
--

INSERT INTO `facturedachat` (`idAchat`, `dateAchat`, `nomAFournisseur`, `libelleFacture`, `montantAHT`, `tvaA`, `dateGenerationExcel`, `idEntrepriseLiée`, `numeroDeCompte`, `modePaiement`) VALUES
(1, '2014-05-01', 'Fournisseur1', '50 filets', 200, 20, '2014-05-12', 2, '6010', 'virement'),
(2, '2014-05-01', 'Fournisseur2', '10 cannes', 20000, 20, '2014-05-12', 2, '6020', 'virement'),
(3, '2014-05-02', 'Fournisseur3', '500kg farine', 525, 20, NULL, 3, '6010', 'chèque'),
(4, '2014-05-02', 'Fournisseur4', '100kg levure', 318, 20, NULL, 3, '6010', 'chèque');

-- --------------------------------------------------------

--
-- Structure de la table `facturedachatimmobilisation`
--

CREATE TABLE IF NOT EXISTS `facturedachatimmobilisation` (
  `idImmo` int(50) NOT NULL AUTO_INCREMENT,
  `dateImmo` date DEFAULT NULL,
  `nomIFournisseur` varchar(100) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `libelleFacture` varchar(100) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `montantIHT` double NOT NULL,
  `tvaI` double NOT NULL,
  `dateGenerationExcel` date DEFAULT NULL,
  `idEntrepriseLiée` int(11) NOT NULL,
  `numeroDeCompte` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  `tauxAmort` double NOT NULL,
  `modePaiement` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  PRIMARY KEY (`idImmo`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci AUTO_INCREMENT=3 ;

--
-- Contenu de la table `facturedachatimmobilisation`
--

INSERT INTO `facturedachatimmobilisation` (`idImmo`, `dateImmo`, `nomIFournisseur`, `libelleFacture`, `montantIHT`, `tvaI`, `dateGenerationExcel`, `idEntrepriseLiée`, `numeroDeCompte`, `tauxAmort`, `modePaiement`) VALUES
(2, '2014-05-12', 'concession peugeot', 'voiture peugeot', 20000, 19.6, NULL, 2, '2100', 0.25, 'chèque');

-- --------------------------------------------------------

--
-- Structure de la table `facturedevente`
--

CREATE TABLE IF NOT EXISTS `facturedevente` (
  `idVente` int(50) NOT NULL AUTO_INCREMENT,
  `dateVente` date DEFAULT NULL,
  `nomClient` varchar(100) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `libelleFacture` varchar(100) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `montantVHT` double NOT NULL,
  `tvaV` double NOT NULL,
  `dateGenerationExcel` date DEFAULT NULL,
  `idEntrepriseLiée` varchar(11) COLLATE utf8_unicode_520_ci NOT NULL,
  `numeroDeCompte` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  `modePaiement` varchar(100) COLLATE utf8_unicode_520_ci NOT NULL,
  PRIMARY KEY (`idVente`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `facturedevente`
--

INSERT INTO `facturedevente` (`idVente`, `dateVente`, `nomClient`, `libelleFacture`, `montantVHT`, `tvaV`, `dateGenerationExcel`, `idEntrepriseLiée`, `numeroDeCompte`, `modePaiement`) VALUES
(1, '2014-05-01', 'client1', 'Vente cannes à pêche', 200000, 20, NULL, '2', '7070', 'espèce');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
