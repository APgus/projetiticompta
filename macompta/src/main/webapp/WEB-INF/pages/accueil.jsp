<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <!-- En-t�te de la page -->
        <meta charset="utf-8" />
        <title>Page d'Accueil</title>
     	<link rel="stylesheet" href="<c:url value="css/cssGeneral.css"/>">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
		
    </head>
    <body>
		<!-- Corps de la page -->
		
		<header id="head">
			<a href="accueil"><img id="head_left" src="images/logo.png" alt="Pierre Sulpice" ></a>
			<div id="head_right">
				<jsp:include page="userBox.jsp">
	    			<jsp:param name="pageSelectionnee" value="accueil"/>
				</jsp:include>
			</div>
		</header>
		<aside id="menu">
			<jsp:include page="menu.jsp">
	    		<jsp:param name="pageSelectionnee" value="accueil"/>
			</jsp:include>
		</aside>

		<div id="content">
			<div id="actu">
				<h4>Bienvenue sur "Sulpice Conseil MaComtpa.fr"</h4>
				<p>Besoin d'aide ? Contactez nous au 0622029446 ou essayez <a href="aideClient" >l'aide en Ligne !</a></p>
				</br>
				<p>Vous pouvez également visiter :
				</br>
				<a href="http://www.sulpiceconseil.fr/fr/page/" >notre site vitrine !</a></p>
				<p>ou nous laisser un avis :
				<a href="mailto:augustin.picot@hei.fr"> notre adresse mail !</a></p>

			</div>
		</div>

		<footer>
			
		</footer>
		
	</body>
	
</html>	