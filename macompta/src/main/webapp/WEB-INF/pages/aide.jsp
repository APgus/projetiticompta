<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- En-t?te de la page -->
        <meta charset="utf-8" />
        <title>Page d'Aide</title>
               <link  type="text/css" rel="stylesheet" href="css/cssGeneral.css">
               <link  type="text/css" rel="stylesheet" href="css/cssAide.css">
               <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
				<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
    </head>
    
    <body>
                <!-- Corps de la page -->
                
                <header>
                        <div id="head"><a href="http://www.sulpiceconseil.fr/fr/page/">
                                <img id="head_left" src="images/logo.png" alt="Pierre Sulpice"></a>
                        </div>
                </header>
                
                <div id="contentaide" >
                      <article><p id="aideenligne"> Aide en ligne</p> <a id="lien" onclick="history.back()" >page précédente</a> </article>
                       
                
                        <div id="corpsaide">
                
                        <p id="p1">
                        Problème de connexion:
                        </p>
                        <div id="cadrequestion">
                        <p id="p2">
                        Vérifier que votre login et mot de passe sont correctes.
                        Sinon, contacter le service client de la société au 0612345678
                        </p>
                        </div>
                        
                        <p id="p1">
                        Qu'est-ce que SulpiceCompta.fr:
                        </p>
                        <div id="cadrequestion">
                        <p id="p2">
                        SulpiceCompta.fr est un projet étudiant qui consiste à fournir un site web de comptabilité en ligne à la société Pierre Sulpice Expertise
                        </p>
                        </div>
                                                <p id="p1">
                        Question 3
                        </p>
                        <div id="cadrequestion">
                        <p id="p2">
                        Réponse 3
                        </p>
                        </div>
                        
                        <p id="p1">
                        Question 4
                        </p>
                        <div id="cadrequestion">
                        <p id="p2">
                        Réponse 4
                        </p>
                        </div>
                        
                        <p id="p1">
                        Question 5
                        </p>
                        <div id="cadrequestion">
                        <p id="p2">
                        Réponse 5
                        </p>
                        </div>
                        
                        
                </div>

                </div>

                <footer>
                        
                </footer>
                
        </body>
        
</html>        
