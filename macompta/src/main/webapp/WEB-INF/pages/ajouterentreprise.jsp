<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <!-- En-t�te de la page -->
        <meta charset="utf-8" />
        <title>Saisie de Facture d 'Achat</title>
		<link rel="stylesheet" href="<c:url value="css/cssGeneral.css"/>">
		<link rel="stylesheet" href="<c:url value="css/cssSaisieFacture.css"/>">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
		
    </head>
    
    <body>
		<!-- Corps de la page -->
		
		<header id="head">
			<a href="accueil"><img id="head_left" src="images/logo.png" alt="Pierre Sulpice" ></a>
				 <div id="head_right">
					<jsp:include page="userBox.jsp">
		    			<jsp:param name="pageSelectionnee" value="accueil"/>
					</jsp:include>
				 </div>
			
		</header>
		<aside id="menu">
			<jsp:include page="menu.jsp">
	    		<jsp:param name="pageSelectionnee" value="saisieFactureAchat"/>
			</jsp:include>
		</aside>

		<div id="content">
			
 
				<div id="saisieFactureAchat">
					<h4>Ajout d'une entreprise cliente:</h4>
					<form method="post" id="ajouterentreprise" action="ajouterentreprise" style="height:75%;">
						 <div>
							<label for="nom">Nom de l'entreprise</label>
							<input  id="ajouterentreprise" type="text" id="nom" name="nom" placeholder="nom de l'entreprise" required>
						 </div>
						  <div>
							<label for="login">Login de l'entreprise</label>
							<input  id="ajouterentreprise" type="text" id="login" name="login" placeholder="login de l'entreprise xxxx@xxxx.fr" required>
						</div>
						  <div>
							<label for="motDePasse">Mot de passe de l'entreprise</label>
							<input  id="ajouterentreprise" type="text" id="motDePasse" name="motDePasse" placeholder="mdp de l'entreprise" required>
						</div>
						  <div>
							<label for="adresse">Adresse de l'entreprise</label>
							<input  id="ajouterentreprise" type="text" id="adresse" name="adresse" placeholder="adresse de l'entreprise" required>
						</div>
						  <div>
							<label for="codePostal">Code postal de l'entreprise</label>
							<input  maxlength="5" id="ajouterentreprise" type="number" id="codePostal" min="0" max="99999"name="codePostal" placeholder="CP de l'entreprise XXXXX" required>
						</div>
						  <div>
							<label for="ville">Ville de l'entreprise</label>
							<input  id="ajouterentreprise" type="text" id="ville" name="ville" placeholder="ville de l'entreprise" required>
						</div>
						  <div>
							<label for="telephone">Telephone de l'entreprise</label>
							<input  maxlength="10" id="ajouterentreprise" type="number" min="" max="9999999999" id="telephone" name="telephone" placeholder="tel de l'entreprise XXXXXXXXXX" required>
						</div>
						  <div>
							<label for="secteurDActivite">Profession de l'entreprise</label>
							<input  id="ajouterentreprise" type="text" id="secteurDActivite" name="secteurDActivite" placeholder="prof de l'entreprise" required>
						</div>
						  <div>
								<input type="submit" value="Ajouter">
					 	 </div>
					</form>
				</div>
		</div>

		<footer>
			
		</footer>
		
	</body>
	
</html>	