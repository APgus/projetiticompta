<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <!-- En-tete de la page -->
        <meta charset="utf-8" />
        <title>Page de Connexion</title>
              <link rel="stylesheet" type="text/css" href="css/cssConnexion.css">
                <link rel="icon" href="images/favicon.ico" type="image/x-icon">
				<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    </head>
    
    <body>
                <!-- Corps de la page -->
                
                <header>
                        <div id="head"><a href="http://www.sulpiceconseil.fr/fr/page/">
                                <img id="head_left" src="images/logo.png" alt="Pierre Sulpice" ></a>
                                <a id="lien" href="<c:url value="/aide"/>" >aide en ligne</a>
                        </div>
                </header>
                
                 
                
                <div id="contentconnexion" >
                	<p id="titreConnexion">Entrez vos paramètres de connexion :</p>
                      
	                <div id="corpsconnexion">
	                
	                	<form method="post" action="connexion">
	                	
	                		<div id="login">
								<label  for="login">Identifiant :  <span class="requis">*</span></label>
								<input name="login" type="text" maxlength="100" value="<c:out value="${entrepriseConnectee.login}"/>" required/>
								<span class="erreur">${form.erreurs['login']}</span>
							</div>
							
							<div id="motDePasse">
								<label  for="motDePasse">Mot de passe :  <span class="requis">*</span></label>
								<input name="motDePasse" type="password" value="" maxlength="100" required />
								<span class="erreur">${form.erreurs['motDePasse']}</span>
							</div>
							
							<div id="BoutonConnexion">
								<input value="Connexion" type="submit" />
								<input type="reset" value="Réinitialiser">
							</div >
							<div id="infoconnexion">
								<c:if test="${empty erreurconnexion}">
									 <span id="messageConnexion"><c:out value="${successconnexion}"/></span>
								</c:if>
							    	<span id="messageErreurConnexion"><c:out value="${erreurconnexion}"/></span>
							 </div>
						</form>
	
	                </div>
						
                    </div>

                <footer>
                        
                </footer>
                
        </body>
        
</html>        
