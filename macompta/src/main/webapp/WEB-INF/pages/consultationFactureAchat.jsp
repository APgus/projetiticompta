<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <!-- En-t�te de la page -->
        <meta charset="utf-8" />
        <title>Factures d'achat</title>
     	<link rel="stylesheet" href="<c:url value="css/cssGeneral.css"/>">
     	<link rel="stylesheet" href="<c:url value="css/cssTable.css"/>">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
		
    </head>
    <body>
		<!-- Corps de la page -->
		
		<header id="head">
			<a href="accueil"><img id="head_left" src="images/logo.png" alt="Pierre Sulpice" ></a>
			<div id="head_right">
				<jsp:include page="userBox.jsp">
	    			<jsp:param name="pageSelectionnee" value="ConsultfactureAchat"/>
				</jsp:include>
			</div>
		</header>
		<aside id="menu">
			<jsp:include page="menu.jsp">
	    		<jsp:param name="pageSelectionnee" value="accueil"/>
			</jsp:include>
		</aside>
		
		<div id="content">
			<div id="choixDate" >
				<form  method="post" action="consultationFactureAchat">
						<div>
							<label for="dateinferieureachat">Afficher les factures comprisent entre le :</label>
							<input type="date" id="dateinferieureachat" name="dateinferieureachat" placeholder="date AAAA-MM-JJ" required>
						</div>
						<div>
							<label for="datesuperieureachat">et le :</label>
							<input type="date" id="datesuperieureachat" name=datesuperieureachat placeholder="date AAAA-MM-JJ" required>
						</div>
						<input type="submit" value="Valider les dates">
				</form>
				<form  method="post" action="reinitialiserDateAchat">
						<input type="submit" value="Réinitialiser les dates">
				</form>	
			</div>
		
			<div class="CSSTable" >
				<table class="tablefactureachat">
						<thead>
							<tr>
								<th>Date d'achat</th>
								<th>Nom du Fournisseur</th>
								<th>Libelle de Facture</th>
								<th>Montant HT</th>
								<th>TVA</th>
								<th>Date de Generation Excel</th>
								<th>Mode de paiement</th>
								<th>Supprimer la facture</th>
								<th>Modifier la facture</th>
								<th>Annuler Génération de la facture</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${datesuperieureachat!=null && dateinferieureachat!=null}">
									<c:forEach var="factureachatconsult" items="${listefactureachat}">
								      	<c:if test="${factureachatconsult.dateAchat.before(datesuperieureachat) || factureachatconsult.dateAchat.equals(datesuperieureachat)}">
											<c:if test="${factureachatconsult.dateAchat.after(dateinferieureachat) || factureachatconsult.dateAchat.equals(dateinferieureachat) }">
											<tr>
											
												<td>${factureachatconsult.dateAchat}</td>
												<td>${factureachatconsult.nomAFournisseur}</td>
												<td>${factureachatconsult.libelleFacture}</td>
												<td>${factureachatconsult.montantAHT}</td>
												<td>${factureachatconsult.tvaA}</td>
												<td>
													<c:if test="${factureachatconsult.dateAGenerationEXCEL!=null}">
													${factureachatconsult.dateAGenerationEXCEL}
													</c:if>
													<c:if test="${factureachatconsult.dateAGenerationEXCEL==null}">
													pas encore générée
													</c:if>
												</td>
												<td>${factureachatconsult.modePaiement}</td>
												<td><a id="deletefacture" href="supprimerFactureAchat?idAchat=${factureachatconsult.idAchat}" onclick="return confirm('Voulez vous vraiment supprimer la facture ?');" >supprimer</a></td>
												<td><a href="modifierFactureAchat?idAchat=${factureachatconsult.idAchat}" onclick="return confirm('Voulez vous vraiment modifier la facture ?');" >modifier</a></td>
												<td><a  id="deletedate" href="enleverDateGenerationAchatExcel?idAchat=${factureachatconsult.idAchat}" onclick="return confirm('Voulez vous vraiment supprimer la date de Génération du Fichier Excel ?');" >annuler</a></td>
											
											</tr>
											</c:if>
										</c:if>
									</c:forEach>
							    </c:when>
						    	<c:otherwise>
							       <c:forEach var="factureachatconsult" items="${listefactureachat}">
											<tr>
											
												<td>${factureachatconsult.dateAchat}</td>
												<td>${factureachatconsult.nomAFournisseur}</td>
												<td>${factureachatconsult.libelleFacture}</td>
												<td>${factureachatconsult.montantAHT}</td>
												<td>${factureachatconsult.tvaA}</td>
												<td>
													<c:if test="${factureachatconsult.dateAGenerationEXCEL!=null}">
													${factureachatconsult.dateAGenerationEXCEL}
													</c:if>
													<c:if test="${factureachatconsult.dateAGenerationEXCEL==null}">
													pas encore générée
													</c:if>
												</td>
												<td>${factureachatconsult.modePaiement}</td>
												<td><a id="deletefacture" href="supprimerFactureAchat?idAchat=${factureachatconsult.idAchat}" onclick="return confirm('Voulez vous vraiment supprimer la facture ?');" >supprimer</a></td>
												<td><a href="modifierFactureAchat?idAchat=${factureachatconsult.idAchat}" onclick="return confirm('Voulez vous vraiment modifier la facture ?');" >modifier</a></td>
												<td><a  id="deletedate" href="enleverDateGenerationAchatExcel?idAchat=${factureachatconsult.idAchat}" onclick="return confirm('Voulez vous vraiment supprimer la date de Génération du Fichier Excel ?');" >annuler</a></td>
											
											</tr>
									</c:forEach>
							    </c:otherwise>
							</c:choose>
							
						</tbody>
					</table>
			</div>
		
		</div>
		<footer>
			
		</footer>
		
	</body>
	
</html>	