<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <!-- En-t�te de la page -->
        <meta charset="utf-8" />
        <title>Factures d'immobilisations</title>
     	<link rel="stylesheet" href="<c:url value="css/cssGeneral.css"/>">
     	<link rel="stylesheet" href="<c:url value="css/cssTable.css"/>">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
		
    </head>
    <body>
		<!-- Corps de la page -->
		
		<header id="head">
			<a href="accueil"><img id="head_left" src="images/logo.png" alt="Pierre Sulpice" ></a>
			<div id="head_right">
				<jsp:include page="userBox.jsp">
	    			<jsp:param name="pageSelectionnee" value="ConsultfactureImmo"/>
				</jsp:include>
			</div>
		</header>
		<aside id="menu">
			<jsp:include page="menu.jsp">
	    		<jsp:param name="pageSelectionnee" value="accueil"/>
			</jsp:include>
		</aside>

		<div id="content">
			<div id="choixDate" >
				<form  method="post" action="consultationFactureImmo">
						<div>
							<label for="dateinferieureimmo">Afficher les factures comprisent entre le :</label>
							<input type="date" id="dateinferieureimmo" name="dateinferieureimmo" placeholder="date AAAA-MM-JJ" required>
						</div>
						<div>
							<label for="datesuperieureimmo">et le :</label>
							<input type="date" id="datesuperieureimmo" name="datesuperieureimmo" placeholder="date AAAA-MM-JJ" required>
						</div>
						<input type="submit" value="Valider les dates">
				</form>
				<form  method="post" action="reinitialiserDateImmo">
						<input type="submit" value="Réinitialiser les dates">
				</form>	
			</div>
		<div class="CSSTable" >
			<table class="tablefactureimmo">
					<thead>
						<tr>
							<th>Date d'achat</th>
							<th>Nom du Fournisseur</th>
							<th>Libelle de Facture</th>
							<th>Montant HT</th>
							<th>TVA</th>
							<th>Date de Generation Excel</th>
							<th>taux d'immobilisation</th>
							<th>Mode de paiement</th>
							<th>Supprimer la facture</th>
							<th>Modifier la facture</th>
							<th>Annuler Génération de la facture</th>
						</tr>
					</thead>
					<tbody>
							<c:choose>
								<c:when test="${datesuperieureimmo!=null && dateinferieureimmo!=null}">
									<c:forEach var="factureimmoconsult" items="${listefactureimmo}">
								      	<c:if test="${factureimmoconsult.dateImmo.before(datesuperieureimmo) || factureimmoconsult.dateImmo.equals(datesuperieureimmo)}">
											<c:if test="${factureimmoconsult.dateImmo.after(dateinferieureimmo) || factureimmoconsult.dateImmo.equals(dateinferieureimmo) }">
												<tr >
													
													<td>${factureimmoconsult.dateImmo}</td>
													<td>${factureimmoconsult.nomIFournisseur}</td>
													<td>${factureimmoconsult.libelleFacture}</td>
													<td>${factureimmoconsult.montantIHT}</td>
													<td>${factureimmoconsult.tvaI}</td>
													<td>
														<c:if test="${factureimmoconsult.dateIGenerationEXCEL!=null}">
														${factureimmoconsult.dateIGenerationEXCEL}
														</c:if>
														<c:if test="${factureimmoconsult.dateIGenerationEXCEL==null}">
														pas encore générée
														</c:if>
													</td>
													<td>${factureimmoconsult.tauxAmort}</td>
													<td>${factureimmoconsult.modePaiement}</td>
													<td><a id="deletefacture" href="supprimerFactureImmo?idImmo=${factureimmoconsult.idImmo}" onclick="return confirm('Voulez vous vraiment supprimer la facture ?');" >supprimer</a></td>
													<td><a href="modifierFactureImmo?idImmo=${factureimmoconsult.idImmo}" onclick="return confirm('Voulez vous vraiment modifier la facture ?');" >modifier</a></td>
													<td><a  id="deletedate" href="enleverDateGenerationImmoExcel?idImmo=${factureimmoconsult.idImmo}" onclick="return confirm('Voulez vous vraiment supprimer la date de Génération du Fichier Excel ?');" >annuler</a></td>
													
												</tr>
											</c:if>
										</c:if>
									</c:forEach>
								</c:when>
						    	<c:otherwise>
						    		<c:forEach var="factureimmoconsult" items="${listefactureimmo}">
						    					<tr >
													
													<td>${factureimmoconsult.dateImmo}</td>
													<td>${factureimmoconsult.nomIFournisseur}</td>
													<td>${factureimmoconsult.libelleFacture}</td>
													<td>${factureimmoconsult.montantIHT}</td>
													<td>${factureimmoconsult.tvaI}</td>
													<td>
														<c:if test="${factureimmoconsult.dateIGenerationEXCEL!=null}">
														${factureimmoconsult.dateIGenerationEXCEL}
														</c:if>
														<c:if test="${factureimmoconsult.dateIGenerationEXCEL==null}">
														pas encore générée
														</c:if>
													</td>
													<td>${factureimmoconsult.tauxAmort}</td>
													<td>${factureimmoconsult.modePaiement}</td>
													<td><a id="deletefacture" href="supprimerFactureImmo?idImmo=${factureimmoconsult.idImmo}" onclick="return confirm('Voulez vous vraiment supprimer la facture ?');" >supprimer</a></td>
													<td><a href="modifierFactureImmo?idImmo=${factureimmoconsult.idImmo}" onclick="return confirm('Voulez vous vraiment modifier la facture ?');" >modifier</a></td>
													<td><a  id="deletedate" href="enleverDateGenerationImmoExcel?idImmo=${factureimmoconsult.idImmo}" onclick="return confirm('Voulez vous vraiment supprimer la date de Génération du Fichier Excel ?');" >annuler</a></td>
													
												</tr>
									</c:forEach>
						    	</c:otherwise>
						    </c:choose>
					</tbody>
				</table>
		</div>
		</div>
		<footer>
			
		</footer>
		
	</body>
	
</html>	