<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <!-- En-t�te de la page -->
        <meta charset="utf-8" />
        <title>Factures de Vente</title>
     	<link rel="stylesheet" href="<c:url value="css/cssGeneral.css"/>">
     	<link rel="stylesheet" href="<c:url value="css/cssTable.css"/>">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
		
    </head>
    <body>
		<!-- Corps de la page -->
		
		<header id="head">
			<a href="accueil"><img id="head_left" src="images/logo.png" alt="Pierre Sulpice" ></a>
			<div id="head_right">
				<jsp:include page="userBox.jsp">
	    			<jsp:param name="pageSelectionnee" value="ConsultfactureVente"/>
				</jsp:include>
			</div>
		</header>
		<aside id="menu">
			<jsp:include page="menu.jsp">
	    		<jsp:param name="pageSelectionnee" value="consultationFactureVente"/>
			</jsp:include>
		</aside>

		<div id="content">
		
			<div id="choixDate" >
				<form  method="post" action="consultationFactureVente">
						<div>
							<label for="dateinferieurevente">Afficher les factures comprisent entre le :</label>
							<input type="date" id="dateinferieurevente" name="dateinferieurevente" placeholder="date AAAA-MM-JJ" required>
						</div>
						<div>
							<label for="datesuperieurevente">et le :</label>
							<input type="date" id="datesuperieurevente" name=datesuperieurevente placeholder="date AAAA-MM-JJ" required>
						</div>
						<input type="submit" value="Valider les dates">
				</form>
				<form  method="post" action="reinitialiserDateVente">
						<input type="submit" value="Réinitialiser les dates">
				</form>	
			</div>
		
		<div class="CSSTable" >
			<table class="tablefacturevente">
					<thead>
						<tr>
							<th>Date de vente</th>
							<th>Nom du Client</th>
							<th>Libelle de Facture</th>
							<th>Montant HT</th>
							<th>TVA</th>
							<th>Date de Generation Excel</th>
							<th>Mode de paiement</th>
							<th>Supprimer la facture</th>
							<th>Modifier la facture</th>
							<th>Annuler Génération de la facture</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
								<c:when test="${datesuperieurevente!=null && dateinferieurevente!=null}">
									<c:forEach var="factureventeconsult" items="${listefacturevente}">
								      	<c:if test="${factureventeconsult.dateVente.before(datesuperieurevente) || factureventeconsult.dateVente.equals(datesuperieurevente)}">
											<c:if test="${factureventeconsult.dateVente.after(dateinferieurevente) || factureventeconsult.dateVente.equals(dateinferieurevente) }">
												<tr >
													
													<td>${factureventeconsult.dateVente}</td>
													<td>${factureventeconsult.nomClient}</td>
													<td>${factureventeconsult.libelleFacture}</td>
													<td>${factureventeconsult.montantVHT}</td>
													<td>${factureventeconsult.tvaV}</td>
													<td>
														<c:if test="${factureventeconsult.dateVGenerationEXCEL!=null}">
														${factureventeconsult.dateVGenerationEXCEL}
														</c:if>
														<c:if test="${factureventeconsult.dateVGenerationEXCEL==null}">
														pas encore générée
														</c:if>
													</td>
													<td>${factureventeconsult.modePaiement}</td>
													<td><a id="deletefacture" href="supprimerFactureVente?idVente=${factureventeconsult.idVente}" onclick="return confirm('Voulez vous vraiment supprimer la facture ?');" >supprimer</a></td>
													<td><a href="modifierFactureVente?idVente=${factureventeconsult.idVente}" onclick="return confirm('Voulez vous vraiment modifier la facture ?');" >modifier</a></td>
													<td><a  id="deletedate" href="enleverDateGenerationVenteExcel?idVente=${factureventeconsult.idVente}" onclick="return confirm('Voulez vous vraiment supprimer la date de Génération du Fichier Excel ?');" >annuler</a></td>
													
												</tr>
											</c:if>
										</c:if>
									</c:forEach>
							    </c:when>
						    	<c:otherwise>
							       <c:forEach var="factureventeconsult" items="${listefacturevente}">
							       				<tr >
													
													<td>${factureventeconsult.dateVente}</td>
													<td>${factureventeconsult.nomClient}</td>
													<td>${factureventeconsult.libelleFacture}</td>
													<td>${factureventeconsult.montantVHT}</td>
													<td>${factureventeconsult.tvaV}</td>
													<td>
														<c:if test="${factureventeconsult.dateVGenerationEXCEL!=null}">
														${factureventeconsult.dateVGenerationEXCEL}
														</c:if>
														<c:if test="${factureventeconsult.dateVGenerationEXCEL==null}">
														pas encore générée
														</c:if>
													</td>
													<td>${factureventeconsult.modePaiement}</td>
													<td><a id="deletefacture" href="supprimerFactureVente?idVente=${factureventeconsult.idVente}" onclick="return confirm('Voulez vous vraiment supprimer la facture ?');" >supprimer</a></td>
													<td><a href="modifierFactureVente?idVente=${factureventeconsult.idVente}" onclick="return confirm('Voulez vous vraiment modifier la facture ?');" >modifier</a></td>
													<td><a  id="deletedate" href="enleverDateGenerationVenteExcel?idVente=${factureventeconsult.idVente}" onclick="return confirm('Voulez vous vraiment supprimer la date de Génération du Fichier Excel ?');" >annuler</a></td>
													
												</tr>
							   	   </c:forEach>
							    </c:otherwise>
							</c:choose>   
					</tbody>
				</table>
		</div>
		</div>
		<footer>
			
		</footer>
		
	</body>
	
</html>	