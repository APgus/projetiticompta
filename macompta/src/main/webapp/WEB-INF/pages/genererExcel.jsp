<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <!-- En-t�te de la page -->
        <meta charset="utf-8" />
        <title>Génération de fichiers Excel</title>
		<link rel="stylesheet" href="<c:url value="css/cssGeneral.css"/>">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
		
    </head>
    
    <body>
		<!-- Corps de la page -->
		
		<header id="head">
			<a href="accueil"><img id="head_left" src="images/logo.png" alt="Pierre Sulpice" ></a>
				 <div id="head_right">
					<jsp:include page="userBox.jsp">
		    			<jsp:param name="pageSelectionnee" value="accueil"/>
					</jsp:include>
				 </div>
			
		</header>
		<aside id="menu">
			<jsp:include page="menu.jsp">
	    		<jsp:param name="pageSelectionnee" value="genererExcel"/>
			</jsp:include>
		</aside>

		<div id="content">
			
 
				
				
				<div id="ContentGenerator">

				  		<h3 id="titreGenerator">Générateur de Fichiers Excel :</h3>
				  		<h6>(Pour les factures jamais générées)</h6>
					<div id="subContentGenerator">
						<form method="post" id="generationExcelAchat" action="generationExcelAchat" style="height:75%;">
							<input type="button"  onclick="location.href='generationExcelAchat'" name="generationExcelAchat" value="Génerer les Factures d'Achat"  style="background-color:#E8E8E8 ">  
						</form>
						<form method="post" id="generationExcelAchatImmo" action="generationExcelAchatImmo" style="height:75%;">
							<input type="button"  onclick="location.href='generationExcelAchatImmo'" name="generationExcelAchatImmo" value="Génerer les Factures d'Achat d'Immobilisation"  style="background-color:#E8E8E8 ">  
						</form>
						<form method="post" id="generationExcelVente" action="generationExcelVente" style="height:75%;">
							<input type="button"  onclick="location.href='generationExcelVente'" name="generationExcelVente" value="Génerer les Factures de Vente"  style="background-color:#E8E8E8 ">  
						</form>
					</div>

				</div>
		</div>

		<footer>
			
		</footer>
		
	</body>
	
</html>	