<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<nav id="menu_onglet">
	<ul class=" niveau1">
		<li><a  href="saisieFactureAchat" class="onglet" >Saisie de facture d'Achat</a></li>
		<li><a  href="saisieFactureImmo" class="onglet">Saisie de facture d'Immobilisation</a></li>
		<li><a  href="saisieFactureVente" class="onglet">Saisie de facture de Vente</a></li>
		<li class="sousmenu"><a  href="" onclick="window.location.reload(true); class="onglet">Consultation des saisies</a>
		 		<ul class="niveau2"> 
                  <li><a  href="genererExcel" id="premiersousonglet" class="sousonglet" >Génération de fichier Excel</a></li>
                  <li><a  href="consultationFactureAchat" class="sousonglet" >Liste des factures d'Achat</a></li>
                  <li><a  href="consultationFactureVente" class="sousonglet" >Liste des factures de Vente</a></li>
                  <li><a  href="consultationFactureImmo" class="sousonglet" >Liste des factures d'Immobilisation</a></li>
                  <li><a  href="tvaAnuelle" class="sousonglet" >Calcul de la TVA de l'entreprise</a></li>
               </ul> 
        </li>       
	</ul>
</nav>