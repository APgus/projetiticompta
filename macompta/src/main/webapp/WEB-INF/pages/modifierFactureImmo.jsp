<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <!-- En-t�te de la page -->
        <meta charset="utf-8" />
        <title>Modification de Facture d'Immobilisation</title>
		<link rel="stylesheet" href="<c:url value="css/cssGeneral.css"/>">
		<link rel="stylesheet" href="<c:url value="css/cssSaisieFacture.css"/>">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
		
    </head>
    
    <body>
		<!-- Corps de la page -->
		
		<header id="head">
			<a href="accueil"><img id="head_left" src="images/logo.png" alt="Pierre Sulpice" ></a>
				 <div id="head_right">
					<jsp:include page="userBox.jsp">
		    			<jsp:param name="pageSelectionnee" value="accueil"/>
					</jsp:include>
				 </div>
			
		</header>
		<aside id="menu">
			<jsp:include page="menu.jsp">
	    		<jsp:param name="pageSelectionnee" value="saisieFactureAchat"/>
			</jsp:include>
		</aside>

		<div id="content">
			
 
				<div id="saisieFactureAchat">
					<h4>Modification d'une facture d'Immobilisation:</h4>
					<form method="post" id="modifierFactureImmo" action="modifierFactureImmo" style="height:75%;">
						 <div>
							<input type="hidden" name="idImmo"  value="<c:out value="${factureimmo.idImmo}" />" />   
						</div>
						  <div>
							<input type="hidden" name="dateIGenerationEXCEL"  value="<c:out value="${factureimmo.dateIGenerationEXCEL}" />" />   
						</div>
						 <div>
							<label for="dateImmo">Date de Facture</label>
							<input id="modifierFactureImmo"  type="date" id="dateImmo" name="dateImmo" placeholder="date de Facture AAAA-MM-JJ" required>
						 </div>
						 <div>
							<label for="libelleFacture">libelle de Facture</label>
							<input  id="modifierFactureImmo" type="text" id="libelleFacture" name="libelleFacture" placeholder="libelle de Facture" required>
						 </div>
						 <div>
							<label for="nomIFournisseur">Nom du Fournisseur</label>
							<input type="text" id="nomIFournisseur" name="nomIFournisseur" placeholder="nom du fournisseur" required>
						 </div>
						  <div>
							<label for="modePaiement">Mode de Paiement</label>
						 	<SELECT id="modePaiement" name="modePaiement" size="1" required>
							<OPTION value="espèce">espèces
							<OPTION value="chèque">chèque
							<OPTION value="virement">virement
							<OPTION value="prélèvement">prélèvement
							<OPTION value="effet de commerce">effet de commerce
							</SELECT>
						 </div>
						 <div>
							<label for="numeroDeCompte">Numero du Compte</label>
							<select name="numeroDeCompte">
							 	<c:forEach var="compte" items="${listecompte}">
								<OPTION value="${compte.numeroDeCompte}">${compte.numeroDeCompte} - ${compte.libelleCompte}
								</c:forEach>
							</select>
						 </div>
						 <div>
							<label for="montantIHT">Montant HT (€)</label>
							<input type="number" step="0.01" id="montantIHT" name="montantIHT" placeholder="montant Facture HT" required>
						 </div>
						 <div>
								<label for="tvaA">TVA (%)</label>
								<SELECT id="tvaI" name="tvaI" size="1" required>
								<OPTION value="20.0">20,0
								<OPTION value="19.6">19,6
								<OPTION value="10.0">10,0
								<OPTION value="7.0">7,0
								<OPTION value="5.5">5,5
								<OPTION value="5.0">5,0
								<OPTION value="2.1">2,1
								</SELECT>
						 </div>
						 <div>
							<label for="tauxAmort">rubrique de l'immobilisation</label>
							<SELECT id="tauxAmort" name="tauxAmort" size="1" required>
							<OPTION value="0.1">matériel de bureau
							<OPTION value="0.25">véhicule
							<OPTION value="0.1">outillage
							<OPTION value="0.1">mobilier
							<OPTION value="0.05">brevets
							<OPTION value="0.20">bâtiment
							<OPTION value="0.33">matériel informatique
							<OPTION value="0.2">agencement, installation
							</SELECT>
						 </div>
						 <div>
							<input type="hidden" name="idEntrepriseLiée" value="${sessionScope.entrepriseConnectee.idEntreprise}" >
						 </div>		
								
						 <div>
								<input type="submit" value="Modifier">
					 	 </div>
					</form>
				</div>
				
		</div>

		<footer>
			
		</footer>
		
	</body>
	
</html>	