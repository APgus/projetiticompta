<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <!-- En-t�te de la page -->
        <meta charset="utf-8" />
        <title>Saisie de Facture de Vente</title>
		<link rel="stylesheet" href="<c:url value="css/cssGeneral.css"/>">
		<link rel="stylesheet" href="<c:url value="css/cssSaisieFacture.css"/>">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
		
    </head>
    
    <body>
		<!-- Corps de la page -->
		
		<header id="head">
			<a href="accueil"><img id="head_left" src="images/logo.png" alt="Pierre Sulpice"></a>
				 <div id="head_right">
					<jsp:include page="userBox.jsp">
		    			<jsp:param name="pageSelectionnee" value="accueil"/>
					</jsp:include>
				 </div>
			
		</header>
		<aside id="menu">
			<jsp:include page="menu.jsp">
	    		<jsp:param name="pageSelectionnee" value="saisieFactureVente"/>
			</jsp:include>
		</aside>

		<div id="content">

				<div id="saisieFactureAchat">
					<h4>Saisie des Factures de Ventes:</h4>
					<form method="post" id="ajouterFactureVente" action="saisieFactureVente" style="height:75%;">
					
						 <div>
							<label for="dateVente">Date de Facture</label>
							<input id="ajouterFactureVente"  type="date" id="dateVente" name="dateVente" placeholder="date de Facture AAAA-MM-JJ" required>
						 </div>
						 
						 <div>
							<label for="libelleFacture">libelle de Facture</label>
							<input  id="ajouterFactureVente" type="text" id="libelleFacture" name="libelleFacture" placeholder="libelle de Facture" required>
						 </div>
						 
						 <div>
							<label for="nomClient">Nom du Client</label>
							<input type="text" id="nomClient" name="nomClient" placeholder="nom du Client" required>
						 </div>
						 
						  <div>
							<label for="modePaiement">Mode de Paiement</label>
						 	<SELECT id="modePaiement" name="modePaiement" size="1" required>
							<OPTION value="espèce">espèces
							<OPTION value="chèque">chèque
							<OPTION value="virement">virement
							<OPTION value="prélèvement">prélèvement
							<OPTION value="effet de commerce">effet de commerce
							</SELECT>
						 </div>
						 
						 <div>
							<label for="numeroDeCompte">Numero du Compte</label>
							<select name="numeroDeCompte">
							 	<c:forEach var="compte" items="${listecompte}">
								<OPTION value="${compte.numeroDeCompte}">${compte.numeroDeCompte} - ${compte.libelleCompte}
								</c:forEach>
							</select>
						 </div>
						 
						 <div>
							<label for="montantVHT">Montant HT (€)</label>
							<input type="number" step="0.01"  id="montantVHT" name="montantVHT" placeholder="montant Facture HT" required>
						 </div>
						 
						 <div>
							<label for="tvaV">TVA (%)</label>
							<SELECT id="tvaV" name="tvaV" size="1" required>
							<OPTION value="20.0">20,0
							<OPTION value="19.6">19,6
							<OPTION value="10.0">10,0
							<OPTION value="7.0">7,0
							<OPTION value="5.5">5,5
							<OPTION value="5.0">5,0
							<OPTION value="2.1">2,1
							</SELECT>
						 </div>
						 
						  <div>
							<input type="hidden" name="idEntrepriseLiée" value="${sessionScope.entrepriseConnectee.idEntreprise}" >
						 </div>	
						 
						 <div>
								<input type="submit" value="Ajouter">
					 	 </div>
					</form>
				</div>
				
				<div id="affichageDerniereFactures">
					<article id="DerniereFactures">
						<h4>Votre dernière saisie de facture de vente :</h4>
						<p>Date de Facturation : <c:out value="${dernierefacturevente.dateVente}"></c:out></p>
						<p>Libelle de Facture : <c:out value="${dernierefacturevente.libelleFacture}"></c:out></p>
						<p>Nom du Client : <c:out value="${dernierefacturevente.nomClient}"></c:out></p>
						<p>Mode de Paiement : <c:out value="${dernierefacturevente.modePaiement}"></c:out></p>
						<p>Numero du Compte : <c:out value="${dernierefacturevente.numeroDeCompte}"></c:out></p>
						<p>Montant HT : <c:out value="${dernierefacturevente.montantVHT}"></c:out> €</p>
						<p>TVA : <c:out value="${dernierefacturevente.tvaV}"></c:out> %</p>
						<input type="button" name="supprimerDerniereFacture" value="Supprimer la derniere facture" onclick="location.href='supprimerdernierefacturevente'" style="background-color:#E8E8E8"  > 
						
					</article> 
				</div>
		</div>

		<footer>
			
		</footer>
		
	</body>
	
</html>	