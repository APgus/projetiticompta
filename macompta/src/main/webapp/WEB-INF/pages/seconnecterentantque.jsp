<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <!-- En-t�te de la page -->
        <meta charset="utf-8" />
        <title>Page d'Accueil</title>
     	<link rel="stylesheet" href="<c:url value="css/cssGeneral.css"/>">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
		
    </head>
    <body>
		<!-- Corps de la page -->
		
		<header id="head">
			<a href="accueil"><img id="head_left" src="images/logo.png" alt="Pierre Sulpice" ></a>
			<div id="head_right">
				<jsp:include page="userBox.jsp">
	    			<jsp:param name="pageSelectionnee" value="accueil"/>
				</jsp:include>
			</div>
		</header>
		<aside id="menu">
			<jsp:include page="menu.jsp">
	    		<jsp:param name="pageSelectionnee" value="accueil"/>
			</jsp:include>
		</aside>

		<div id="content">
			<h4>Se connecter en tant que :</h4>
				<div id="seconnecterentantque">
					<form method="post" action="seconnecterentantque" style="height:75%;">
						 <div>
							<label for="login">Nom de l'entreprise</label>
							<select name="login">
							 	<c:forEach var="entreprise" items="${listeentreprise}">
								<OPTION value="${entreprise.login}">${entreprise.nom}
								</c:forEach>
							</select>
						 </div>
						 
						 <input value="Se connecter en tant que" type="submit" />
					</form>
				</div>
		</div>

		<footer>
			
		</footer>
		
	</body>
	
</html>	