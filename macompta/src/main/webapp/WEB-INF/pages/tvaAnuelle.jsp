<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <!-- En-t�te de la page -->
        <meta charset="utf-8" />
        <title>Page d'Accueil</title>
     	<link rel="stylesheet" href="<c:url value="css/cssGeneral.css"/>">
     	<link rel="stylesheet" href="<c:url value="css/cssTable.css"/>">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
		
    </head>
    <body>
		<!-- Corps de la page -->
		
		<header id="head">
			<a href="accueil"><img id="head_left" src="images/logo.png" alt="Pierre Sulpice" ></a>
			<div id="head_right">
				<jsp:include page="userBox.jsp">
	    			<jsp:param name="pageSelectionnee" value="accueil"/>
				</jsp:include>
			</div>
		</header>
		<aside id="menu">
			<jsp:include page="menu.jsp">
	    		<jsp:param name="pageSelectionnee" value="accueil"/>
			</jsp:include>
		</aside>

		<div id="content">
		<div id="choixDate" >
				<form  method="post" action="tvaAnuelle">
						<div>
							<label for="anneetvainf">Calculer la Tva entre le :</label>
							<input type="date" id="anneetvainf" name="anneetvainf" placeholder="date AAAA" required>
						</div>
						<div>
							<label for="anneetvasup">et le :</label>
							<input type="date" id="anneetvasup" name="anneetvasup" placeholder="date AAAA" required>
						</div>
						<input type="submit" value="Valider la période">
				</form>
				<form  method="post" action="reinitialiserDateTva">
						<input type="submit" value="Réinitialiser les dates">
				</form>	
			</div>
		
			<div class="CSSTabletva" >
				<table class="tabletva">
						<thead>
							<tr>
								<th>Tva pour la période d'Achat</th>
								<th>Tva pour la période d'Immobilisation</th>
								<th>Tva pour la période de Vente</th>
								<th>Tva calculée</th>		
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${anneetvasup!=null && anneetvainf!=null}">
											<c:set var="sumachat" value="0"/>
									    	<c:set var="sumvente" value="0"/> 
									    	<c:set var="sumimmo" value="0"/>   					
										    		<c:forEach var="factureachatconsult" items="${listefactureachat}">
										    			<c:if test="${factureachatconsult.dateAchat.before(anneetvasup) || factureachatconsult.dateAchat.equals(anneetvasup)}">
														<c:if test="${factureachatconsult.dateAchat.after(anneetvainf) || factureachatconsult.dateAchat.equals(anneetvainf) }">
										    				<c:set var="sumachat" value="${sumachat + factureachatconsult.montantAHT*factureachatconsult.tvaA/100}"/>
										    			</c:if>
														</c:if>
										    		</c:forEach>
										    		<c:forEach var="factureimmoconsult" items="${listefactureimmo}">
										    			<c:if test="${factureimmoconsult.dateImmo.before(anneetvasup) || factureimmoconsult.dateImmo.equals(anneetvasup)}">
														<c:if test="${factureimmoconsult.dateImmo.after(anneetvainf) || factureimmoconsult.dateImmo.equals(anneetvainf) }">
										    				<c:set var="sumimmo" value="${sumimmo + factureimmoconsult.montantIHT*factureimmoconsult.tvaI/100}"/>
										    			</c:if>
														</c:if>
										    		</c:forEach>	
										    		<c:forEach var="factureventeconsult" items="${listefacturevente}">
										    			<c:if test="${factureventeconsult.dateVente.before(anneetvasup) || factureventeconsult.dateVente.equals(anneetvasup)}">
														<c:if test="${factureventeconsult.dateVente.after(anneetvainf) || factureventeconsult.dateVente.equals(anneetvainf) }">
										    				<c:set var="sumvente" value="${sumvente + factureventeconsult.montantVHT*factureventeconsult.tvaV/100}"/>					    		
										    			</c:if>
														</c:if>
													</c:forEach>
																
											      			<tr>
																<td><c:out value="${sumachat}"/></td>
																<td><c:out value="${sumimmo}"/></td>
																<td><c:out value="${sumvente}"/></td>
																<td><c:out value="${sumvente-sumachat+sumimmo}"/></td>
															</tr>
							    </c:when>
						    	<c:otherwise>
						    		<c:set var="sumachat" value="0"/>
						    		<c:set var="sumvente" value="0"/> 
						    		<c:set var="sumimmo" value="0"/>   
						    		<c:forEach var="factureachatconsult" items="${listefactureachat}">
						    			<c:set var="sumachat" value="${sumachat + factureachatconsult.montantAHT*factureachatconsult.tvaA/100}"/>
						    		</c:forEach>
						    		<c:forEach var="factureimmoconsult" items="${listefactureimmo}">
						    			<c:set var="sumimmo" value="${sumimmo + factureimmoconsult.montantIHT*factureimmoconsult.tvaI/100}"/>
						    		</c:forEach>	
						    		<c:forEach var="factureventeconsult" items="${listefacturevente}">
						    			<c:set var="sumvente" value="${sumvente + factureventeconsult.montantVHT*factureventeconsult.tvaV/100}"/>
						    		</c:forEach>			
							      			<tr>
												<td><c:out value="${sumachat}"/></td>
												<td><c:out value="${sumimmo}"/></td>
												<td><c:out value="${sumvente}"/></td>
												<td><c:out value="${sumvente-sumachat+sumimmo}"/></td>
											</tr>
							    </c:otherwise>
							</c:choose>
						</tbody>
					</table>
				
			</div>
			<div id="resultattva">
				<p>
				<c:if test="${(sumvente-sumachat+sumimmos)>=0 }">
					Le montant total de la TVA collectée pour la période définie (dû à l'état): <c:out value="${sumvente-sumachat+sumimmos}"/> Euros
				</c:if></p>
				<p>
				<c:if test="${(sumvente-sumachat+sumimmos)<0 }">
					Le montant total de la TVA  pour la période définie  dû par l'état: <c:out value="${-(sumvente-sumachat+sumimmos)}"/> Euros
				</c:if></p>
			</div>
		</div>
		<footer>
			
		</footer>
		
	</body>
	
</html>	