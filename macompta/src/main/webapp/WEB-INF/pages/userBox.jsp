<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div>
	<img id="userPict" src='images/user.png' alt='user'>
	<div id="userDef">
		<h5>Nom d'utilisateur : <br/> ${sessionScope.entrepriseConnectee.login}</h5>
		<h5>Type d'utilisateur : 
			<c:choose>
			    <c:when test="${sessionScope.entrepriseConnectee.login == 'pierre.sulpice@sulpiceconseil.fr'}">
			        <!-- traitement -->
			        <br/>
			        Administrateur
			    </c:when>
			    
			    <c:otherwise>
			        <!-- traitement par défaut -->
			        <br/>
			        Client / ${sessionScope.entrepriseConnectee.secteurDActivite}
			    </c:otherwise>
			</c:choose>

		</h5>
		<a class="lienuserbox" href="deconnexion"> Déconnexion </a>
		<c:if test="${sessionScope.entrepriseConnecteeBis.login == 'pierre.sulpice@sulpiceconseil.fr'}">
			<br/>
			<a class="lienuserbox" href="seconnecterentantque"> Se Connecter en tant que </a>
			<br/>
			<a class="lienuserbox" href="ajouterentreprise">Ajouter un Client </a>   
		</c:if>
	</div>
	<c:choose>
			    <c:when test="${sessionScope.entrepriseConnecteeBis.login == 'pierre.sulpice@sulpiceconseil.fr'}">
			        <!-- traitement -->
			       <a  id="aideadminOnline" href="aideClient" > <img id="helpUser" src='images/help.png' alt='user'> aide en ligne</a>
			    </c:when>
			    
			    <c:otherwise>
			        <!-- traitement par défaut -->
			       <a  id="aideOnline" href="aideClient" > <img id="helpUser" src='images/help.png' alt='user'> aide en ligne</a>
			    </c:otherwise>
			</c:choose>
	
	
</div>